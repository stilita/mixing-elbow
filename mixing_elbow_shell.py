#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.6.0 with dump python functionality
###

import sys
import os
import salome



viscous_layers = True
shell_launch = False


if not shell_launch:
	salome.salome_init()
	import salome_notebook
	notebook = salome_notebook.NoteBook()



if shell_launch:
	dir_path = os.path.dirname(os.path.realpath(__file__))
else:
	dir_path = r'/data/Joel/mixing-elbow'
    #dir_path = r'C:\\Users\\claudio.caccia\\Documents\\GitHub\\mixing-elbow'

print(dir_path)


workdir = dir_path  # r'/home/claudio/Projects/mixing_elbow'
sys.path.insert(0, workdir)
os.chdir(workdir)

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import numpy as np
import SALOMEDS

geompy = geomBuilder.New()


#diameter of injection pipe
pipe_diam = 0.2

#angle between x-axis and line connecting origin and insertion point
junction_angle = np.deg2rad(25.0)

#angle between x-axis and injection pipe axis
insert_angle = np.deg2rad(75.0)


# main tube radius
tube_r = 0.5
# lenght of tube before, after
tube_l = 3
# radius of elbow
elbow_r = 1.5
# injection tube length
injection_l = 1


# elbow coordinates:
p_inlet = np.array([elbow_r,-tube_l,0])
p_elbow_start = np.array([elbow_r,0,0])
p_elbow_end = np.array([0,elbow_r,0])
p_outlet = np.array([-tube_l,elbow_r,0])


# injection tube coordinates
# point A: intestection point
pA = (elbow_r+tube_r)*np.array([np.cos(junction_angle),np.sin(junction_angle),0])
#xA = (elbow_r+tube_r)*np.cos(junction_angle)
#yA = (elbow_r+tube_r)*np.sin(junction_angle)


# point B: injection inlet
pB = pA + injection_l*np.array([np.cos(insert_angle),np.sin(insert_angle),0])
#xB = xA + np.cos(insert_angle)
#yB = yA + np.sin(insert_angle)



# build elbow

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
inlet = geompy.MakeVertex(*p_inlet)
elbow_start = geompy.MakeVertex(*p_elbow_start)
elbow_end = geompy.MakeVertex(*p_elbow_end)
outlet = geompy.MakeVertex(*p_outlet)
first_leg = geompy.MakeLineTwoPnt(inlet, elbow_start)
second_leg = geompy.MakeLineTwoPnt(elbow_end, outlet)
Arc_1 = geompy.MakeArcCenter(O, elbow_start, elbow_end,False)
elbow_path = geompy.MakeWire([first_leg, second_leg, Arc_1], 1e-07)
main_section = geompy.MakeDiskPntVecR(inlet, OY, tube_r)
out_section = geompy.MakeDiskPntVecR(outlet, OX, tube_r)
Pipe = geompy.MakePipe(main_section, elbow_path)


inlet_1 = geompy.CreateGroup(Pipe, geompy.ShapeType["FACE"])
outlet_1 = geompy.CreateGroup(Pipe, geompy.ShapeType["FACE"])
Pipe_all_faces = geompy.CreateGroup(Pipe, geompy.ShapeType["FACE"])

inlet_obj = geompy.GetInPlace(Pipe, main_section, True)
inlet_face = geompy.SubShapeAll(inlet_obj, geompy.ShapeType["FACE"])


outlet_obj = geompy.GetInPlace(Pipe, out_section, True)
outlet_face = geompy.SubShapeAll(outlet_obj, geompy.ShapeType["FACE"])

Pipe_face_list = geompy.SubShapeAll(Pipe, geompy.ShapeType["FACE"])

geompy.UnionList(inlet_1, inlet_face)
geompy.UnionList(outlet_1, outlet_face)

geompy.UnionList(Pipe_all_faces, Pipe_face_list)


 #geompy.UnionList(wall, wall_faces)

wall1 = geompy.CutListOfGroups([Pipe_all_faces], [inlet_1, outlet_1])







def find_intersection_with_tubes(m, c, xA, yA, r, tl):
	# look for intersection with vertical tube
	yV = r*m + c
	# look for intersection with horizontal tube
	xH = (r-c)/m
	
	if yV < 0:
		print("intersection with vertical")
		if -yV <= tl:
			L = np.sqrt((r-xA)**2 + (yV-yA)**2)
		else:
			print("intersection with tube too far away")
			L = -1
	elif xH < 0:
		print("intersection with horizontal")
		if -xH <= tl:
			L = np.sqrt((xH-xB)**2 + (1.5-yB)**2)
		else:
			print("intersection with tube too far away")
			L = -1
	else:
		print("no intersection?!?")
		L = -1
	return L


def find_intersection_with_elbow(insert_angle, xA, yA, r, tl):
	# check if injection tube axis is vertical
	if insert_angle < np.pi/2:
		# if not vertical we compute the intersection between the line of the 
		# injection tube and the circle of the elbow 
		# system x^2+y^2 = r_tube^2 and (y-yB)/(x-xB) = m
		m = np.tan(insert_angle)
		c = yA-m*xA
		A = m**2+1
		Bhalf = m*c
		C = (c**2 - r**2)
		Delta = r**2*(m**2+1)-c**2
		if Delta < 0:
			# no intersection 
			# we look for intersection
			print("NO INTERSECTION with ELBOW")
			
			if m == 0:
				print("horizontal line not intersecting circle.. unfeasible")
				L = -1
			else:
				L = find_intersection_with_tubes(m, c, xA, yA, r, tl)
		else:
			# intersection coordinates
			xC = (-Bhalf + np.sqrt(Delta))/A
			yC = m*xC+c
			print("xC = {0}, yC = {1}".format(xC,yC))
			if xC >= 0 and yC >= 0:
				# intersection in the first quadrant
				L =np.sqrt((xC-xA)**2 + (yC-yA)**2)
			else:
				# intersection in other quadrants: we look for intersection with tubes
				print("INTERSECTION IN OTHER QUADRANTS")
				L = find_intersection_with_tubes(m, c, xA, yA, r, tl)
	else:
		print("vertical")
		if xA > r:
			print("xA = {0}, r ={1} NO INTERSECTION with ELBOW... Unfeasible".format(xA,r))
			L = -1
		else:
			# find intersection with circle
			yI = np.sqrt(r**2-xA**2)
			L = yA - yI
	
	return L



insertion_point = geompy.MakeVertex(*pA)
inlet2 = geompy.MakeVertex(*pB)
insert_dir = geompy.MakeVector(inlet2, insertion_point)
mix_section = geompy.MakeDiskPntVecR(inlet2, insert_dir, pipe_diam/2)



L1 = find_intersection_with_elbow(insert_angle, pA[0], pA[1], elbow_r, tube_l)

if L1 < 0:
	sys.exit()


L = L1+injection_l-pipe_diam/2

print("L = {0}".format(L))


mix_tube = geompy.MakeCylinder(inlet2, insert_dir, pipe_diam/2, L)

# create empty groups for inlet2 and ALL
inlet2_1 = geompy.CreateGroup(mix_tube, geompy.ShapeType["FACE"])
mix_tube_all_faces = geompy.CreateGroup(mix_tube, geompy.ShapeType["FACE"])


# create object and face for inlet2
inlet2_obj = geompy.GetInPlace(mix_tube, mix_section, True)
inlet2_face = geompy.SubShapeAll(inlet2_obj, geompy.ShapeType["FACE"])

# get all faces of tube
mix_tube_face_list = geompy.SubShapeAll(mix_tube, geompy.ShapeType["FACE"])

#union of 
geompy.UnionList(inlet2_1, inlet2_face)
geompy.UnionList(mix_tube_all_faces, mix_tube_face_list)

wall2 = geompy.CutListOfGroups([mix_tube_all_faces], [inlet2_1])


mixing_elbow = geompy.MakeFuseList([Pipe, mix_tube], True, True)


try:
	geompy.ExportSTL(mixing_elbow, workdir+"/mixing_elbow.stl", True, 0.0001, True)
	print('STL exported')
except:
	print('ExportSTL() failed.')


if True: #not shell_launch:
	geompy.addToStudy( O, 'O' )
	geompy.addToStudy( OX, 'OX' )
	geompy.addToStudy( OY, 'OY' )
	geompy.addToStudy( OZ, 'OZ' )
	geompy.addToStudy( inlet, 'inlet' )
	geompy.addToStudy( elbow_start, 'elbow_start' )
	geompy.addToStudy( elbow_end, 'elbow_end' )
	geompy.addToStudy( outlet, 'outlet' )
	geompy.addToStudy( first_leg, 'first_leg' )
	geompy.addToStudy( second_leg, 'second_leg' )
	geompy.addToStudy( Arc_1, 'Arc_1' )
	geompy.addToStudy( elbow_path, 'elbow_path' )
	geompy.addToStudy( main_section, 'main_section' )
	geompy.addToStudy( out_section, 'out_section' )
	geompy.addToStudy( Pipe, 'Pipe' )
	geompy.addToStudy( insertion_point, 'insertion_point' )
	geompy.addToStudy( inlet2, 'inlet2' )
	geompy.addToStudy( insert_dir, 'insert_dir' )
	geompy.addToStudy( mix_section, 'mix_section' )
	geompy.addToStudy( mix_tube, 'mix_tube' )
	geompy.addToStudy( mixing_elbow, 'mixing_elbow' )

	geompy.addToStudyInFather( Pipe, inlet_1, 'inlet' )
	geompy.addToStudyInFather( Pipe, outlet_1, 'outlet' )
	geompy.addToStudyInFather( Pipe, wall1, 'wall1' )

	geompy.addToStudyInFather( mix_tube, inlet2_1, 'inlet2' )
	geompy.addToStudyInFather( mix_tube, wall2, 'wall2' )




#[inlet_3, outlet_3, wall1_1, inlet2_3, wall2_1] = geompy.RestoreGivenSubShapes(mixing_elbow, [inlet_1, outlet_1, wall1, inlet2_1, wall2], GEOM.FSM_GetInPlace, False, False)
[inlet_3, outlet_3, wall1_1, Pipe_1, inlet2_3, wall2_1, mix_tube_1] = geompy.RestoreGivenSubShapes(mixing_elbow, [inlet_1, outlet_1, wall1, Pipe, inlet2_1, wall2, mix_tube], GEOM.FSM_GetInPlace, False, False)
[inlet_3, outlet_3, wall1_1, inlet2_3, wall2_1] = geompy.GetExistingSubObjects(mixing_elbow, False)

###
### SMESH component
###


import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()
smesh.SetEnablePublish( not shell_launch ) # Set to False to avoid publish in study if not needed or in some particular situations:
                                 # multiples meshes built in parallel, complex and numerous mesh edition (performance)

Hp01 = smesh.CreateHypothesis('NETGEN_Parameters', 'NETGENEngine')
NETGEN_1D_2D_3D = smesh.CreateHypothesis('NETGEN_2D3D', 'NETGENEngine')
Hp01.SetMaxSize( 0.16 )
Hp01.SetMinSize( 5e-04 )
Hp01.SetSecondOrder( 0 )
Hp01.SetOptimize( 1 )
Hp01.SetFineness( 5 )
Hp01.SetGrowthRate( 0.3 )
Hp01.SetNbSegPerEdge( 3 )
Hp01.SetNbSegPerRadius( 7 )
Hp01.SetChordalError( -1 )
Hp01.SetChordalErrorEnabled( 0 )
Hp01.SetUseSurfaceCurvature( 1 )
Hp01.SetFuseEdges( 1 )
Hp01.SetQuadAllowed( 0 )
Hp01.SetCheckChartBoundary( 248 )


VL1 = smesh.CreateHypothesis('ViscousLayers')
VL1.SetTotalThickness( 0.05*tube_r )
VL1.SetNumberLayers( 3 )
VL1.SetStretchFactor( 1.2 )
VL1.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH )

#VL1.SetFaces( [ 7, 12, 20 ], 0 )
VL1.SetFaces(wall1_1.GetSubShapeIndices(), 0 )

#print("IDs of wall 1")
#print(wall1_1.GetSubShapeIndices())
#print(geompy.GetObjectIDs(wall1_1))

VL2 = smesh.CreateHypothesis('ViscousLayers')
VL2.SetTotalThickness( 0.05*pipe_diam/2 )
VL2.SetNumberLayers( 3 )
VL2.SetStretchFactor( 1.2 )
VL2.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH )

#VL2.SetFaces( [ 25 ], 0 )
VL2.SetFaces(wall2_1.GetSubShapeIndices(), 0 )

#print("IDs of wall 2")
#print(wall2_1.GetSubShapeIndices())
#print(geompy.GetObjectIDs(wall2_1))


me = smesh.Mesh(mixing_elbow)
status = me.AddHypothesis(Hp01)
status = me.AddHypothesis(NETGEN_1D_2D_3D)
inlet_me = me.GroupOnGeom(inlet_3,'inlet',SMESH.FACE)
outlet_me = me.GroupOnGeom(outlet_3,'outlet',SMESH.FACE)
wall1_me = me.GroupOnGeom(wall1_1,'wall1',SMESH.FACE)
inlet2_me = me.GroupOnGeom(inlet2_3,'inlet2',SMESH.FACE)
wall2_me = me.GroupOnGeom(wall2_1,'wall2',SMESH.FACE)


me_vl = smesh.Mesh(mixing_elbow)
status = me_vl.AddHypothesis(Hp01)
status = me_vl.AddHypothesis(VL2)
status = me_vl.AddHypothesis(VL1)
status = me_vl.AddHypothesis(NETGEN_1D_2D_3D)
inlet_mevl = me_vl.GroupOnGeom(inlet_3,'inlet',SMESH.FACE)
outlet_mevl = me_vl.GroupOnGeom(outlet_3,'outlet',SMESH.FACE)
wall1_mevl = me_vl.GroupOnGeom(wall1_1,'wall1',SMESH.FACE)
inlet2_mevl = me_vl.GroupOnGeom(inlet2_3,'inlet2',SMESH.FACE)
wall2_mevl = me_vl.GroupOnGeom(wall2_1,'wall2',SMESH.FACE)



if not viscous_layers:
	print("Exporting UNV without viscous layers...")
	isDone = me.Compute()
	[ inlet_me, outlet_me, wall1_me, inlet2_me, wall2_me ] = me.GetGroups()
	if isDone:
		try:
			me.ExportUNV( workdir+'/mesh_elbow.unv' )
			print('UNV exported')
		except:
			print('ExportUNV() failed.')
	else:
		print("Mixing Elbow meshing failed")
else:
	print("Exporting UNV with viscous layers...")
	isDone = me_vl.Compute()
	[ inlet_mevl, outlet_mevl, wall1_mevl, inlet2_mevl, wall2_mevl ] = me_vl.GetGroups()
	if isDone:
		try:
			me_vl.ExportUNV( workdir+'/mesh_elbow_vl.unv' )
			print('UNV with VL exported')
		except:
			print('ExportUNV() with VL failed.')
	else:
		print("Mixing Elbow meshing with viscous layers failed")



## Set names of Mesh objects

if not shell_launch:
	smesh.SetName(NETGEN_1D_2D_3D, 'NETGEN 1D-2D-3D')
	smesh.SetName(VL1, 'VL1')
	smesh.SetName(VL2, 'VL2')
	smesh.SetName(Hp01, 'Hp01')
	smesh.SetName(me.GetMesh(), 'me')
	smesh.SetName(me_vl.GetMesh(), 'me_vl')
	smesh.SetName(inlet_me, 'inlet')
	smesh.SetName(inlet_mevl, 'inlet')
	smesh.SetName(inlet2_me, 'inlet2')
	smesh.SetName(inlet2_mevl, 'inlet2')
	smesh.SetName(outlet_me, 'outlet')
	smesh.SetName(outlet_mevl, 'outlet')
	smesh.SetName(wall1_me, 'wall1')
	smesh.SetName(wall1_mevl, 'wall1')
	smesh.SetName(wall2_me, 'wall2')
	smesh.SetName(wall2_mevl, 'wall2')



if salome.sg.hasDesktop():
	salome.sg.updateObjBrowser()
