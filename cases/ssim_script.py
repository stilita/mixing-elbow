#!/usr/bin/env python
# coding: utf-8

import getopt
import sys

import numpy as np
import cv2 as cv

import tensorflow as tf


def main(argv):
    try:
        opts, args = getopt.getopt(argv, "hf:b:", ["help","file=","benchmark="])
    except getopt.GetoptError:
        print('invalid option:')
        print('ssim_script.py -f image.png [-b benchmark.png]')
        sys.exit(2)
        
        # default benchmark file
        benchfile = "image_bench.png"
        
    for opt, arg in opts:
        if opt in ("-h","--help"):
            print("usage:")
            print("ssim_script.py -f image.png [-b benchmark.png]")
            sys.exit()
        elif opt in ("-f", "--file"):
            inputfile = arg
        elif opt in ("-b","--benchmark"):
            benchfile = arg
             
    np.set_printoptions(precision=7)
    
    ssim_value(inputfile, benchfile)



def get_center(image):

    # make grayscale
    img = cv.cvtColor(image, cv.COLOR_RGB2GRAY)
    #blur
    img = cv.medianBlur(img, 5)
    # look for circles
    circles = cv.HoughCircles(img, cv.HOUGH_GRADIENT, 1, 150, 
                              param1=50, param2=30, minRadius=200, maxRadius=0)
    
    # round to pixel
    circles = np.uint16(np.around(circles))

    cx=circles[0][0][0]
    cy=circles[0][0][1]
    radius=circles[0][0][2]
    
    return cx, cy ,radius




def ssim_value(inputfile, benchfile):
    
    # read color image 
    im = cv.imread(benchfile, cv.IMREAD_COLOR)
        
    cx, cy, radius = get_center(im)
    
    R = radius+2
    im_bench=im[cy-R:cy+R,cx-R:cx+R,:]
    
    
    im_01_full = cv.imread(inputfile, cv.IMREAD_COLOR)
    im_01=im_01_full[cy-R:cy+R,cx-R:cx+R,:]
    
    #im_bench = tf.image.decode_image(tf.io.read_file('image_bench.png'))
    #im_01 = tf.image.decode_image(tf.io.read_file('image_01.png'))
    tfim_bench = tf.convert_to_tensor(np.expand_dims(im_bench, 0))
    tfim_01 = tf.convert_to_tensor(np.expand_dims(im_01, 0))


    # Add an outer batch for each image.
    #im_bench = tf.expand_dims(im_bench, axis=0)
    #im_01 = tf.expand_dims(im_01, axis=0)

    # Compute SSIM over tf.uint8 Tensors.
    ssim1 = tf.image.ssim(tfim_bench, tfim_01, max_val=255, filter_size=11, filter_sigma=1.5, k1=0.01, k2=0.03)

    value = ssim1.numpy()[0]
    
    print(value)
    
    return value


# In[ ]:


if __name__ == "__main__":
    main(sys.argv[1:])

