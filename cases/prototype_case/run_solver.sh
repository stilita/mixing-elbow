#!/bin/bash
cd ${0%/*} || exit 1    		    		# Run from this directory
. $WM_PROJECT_DIR/bin/tools/RunFunctions    # Tutorial run functions


foamCleanTutorials
ideasUnvToFoam ./mesh/mesh_elbow_vl.unv
changeDictionary
checkMesh	| tee log.checkMesh

#topoSet
#pisoFoam 	| tee log.pisofoam

solver=$(getApplication)
procs=$(getNumberOfProcessors)

echo $solver
echo $procs


decomposePar -force | tee log.decomposePar
mpirun -np $procs renumberMesh -overwrite -parallel | tee log.renumber
mpirun -np $procs $solver -parallel  2>&1 | tee log.pimpleFoam


