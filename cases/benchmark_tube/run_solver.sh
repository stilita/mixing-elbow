#!/bin/bash
cd ${0%/*} || exit 1    		    		# Run from this directory
. $WM_PROJECT_DIR/bin/tools/RunFunctions    # Tutorial run functions

foamCleanTutorials
ideasUnvToFoam ./mesh/tube20.unv
changeDictionary
checkMesh	| tee log.checkMesh

#topoSet
#pisoFoam 	| tee log.pisofoam

procs=$(getNumberOfProcessors)

decomposePar | tee log.decomposePar
mpirun -np $procs renumberMesh -parallel -overwrite | tee log.renumber
mpirun -np $procs pimpleFoam -parallel | tee log.pimpleFoam
