# trace generated using paraview version 5.8.0
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'OpenFOAMReader'
benchmark_case_s1_of2012foam = OpenFOAMReader(FileName='/data/Joel/mixing-elbow/cases/benchmark_tube/benchmark_tube.foam')
benchmark_case_s1_of2012foam.SkipZeroTime = 1
benchmark_case_s1_of2012foam.CaseType = 'Reconstructed Case'
benchmark_case_s1_of2012foam.LabelSize = '32-bit'
benchmark_case_s1_of2012foam.ScalarSize = '64-bit (DP)'
benchmark_case_s1_of2012foam.Createcelltopointfiltereddata = 1
benchmark_case_s1_of2012foam.Adddimensionalunitstoarraynames = 0
benchmark_case_s1_of2012foam.MeshRegions = ['internalMesh']
benchmark_case_s1_of2012foam.CellArrays = ['U', 'p', 's1', 'yPlus']
benchmark_case_s1_of2012foam.PointArrays = []
benchmark_case_s1_of2012foam.LagrangianArrays = []
benchmark_case_s1_of2012foam.Cachemesh = 1
benchmark_case_s1_of2012foam.Decomposepolyhedra = 1
benchmark_case_s1_of2012foam.ListtimestepsaccordingtocontrolDict = 0
benchmark_case_s1_of2012foam.Lagrangianpositionswithoutextradata = 1
benchmark_case_s1_of2012foam.Readzones = 0
benchmark_case_s1_of2012foam.Copydatatocellzones = 0

# get animation scene
animationScene1 = GetAnimationScene()

# get the time-keeper
timeKeeper1 = GetTimeKeeper()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# Properties modified on benchmark_case_s1_of2012foam
benchmark_case_s1_of2012foam.CaseType = 'Decomposed Case'

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [1603, 849]

# get layout
layout1 = GetLayout()

# show data in view
benchmark_case_s1_of2012foamDisplay = Show(benchmark_case_s1_of2012foam, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'p'
pLUT = GetColorTransferFunction('p')

# get opacity transfer function/opacity map for 'p'
pPWF = GetOpacityTransferFunction('p')

# trace defaults for the display properties.
benchmark_case_s1_of2012foamDisplay.Representation = 'Surface'
benchmark_case_s1_of2012foamDisplay.ColorArrayName = ['POINTS', 'p']
benchmark_case_s1_of2012foamDisplay.LookupTable = pLUT
benchmark_case_s1_of2012foamDisplay.MapScalars = 1
benchmark_case_s1_of2012foamDisplay.MultiComponentsMapping = 0
benchmark_case_s1_of2012foamDisplay.InterpolateScalarsBeforeMapping = 1
benchmark_case_s1_of2012foamDisplay.Opacity = 1.0
benchmark_case_s1_of2012foamDisplay.PointSize = 2.0
benchmark_case_s1_of2012foamDisplay.LineWidth = 1.0
benchmark_case_s1_of2012foamDisplay.RenderLinesAsTubes = 0
benchmark_case_s1_of2012foamDisplay.RenderPointsAsSpheres = 0
benchmark_case_s1_of2012foamDisplay.Interpolation = 'Gouraud'
benchmark_case_s1_of2012foamDisplay.Specular = 0.0
benchmark_case_s1_of2012foamDisplay.SpecularColor = [1.0, 1.0, 1.0]
benchmark_case_s1_of2012foamDisplay.SpecularPower = 100.0
benchmark_case_s1_of2012foamDisplay.Luminosity = 0.0
benchmark_case_s1_of2012foamDisplay.Ambient = 0.0
benchmark_case_s1_of2012foamDisplay.Diffuse = 1.0
benchmark_case_s1_of2012foamDisplay.Roughness = 0.3
benchmark_case_s1_of2012foamDisplay.Metallic = 0.0
benchmark_case_s1_of2012foamDisplay.Texture = None
benchmark_case_s1_of2012foamDisplay.RepeatTextures = 1
benchmark_case_s1_of2012foamDisplay.InterpolateTextures = 0
benchmark_case_s1_of2012foamDisplay.SeamlessU = 0
benchmark_case_s1_of2012foamDisplay.SeamlessV = 0
benchmark_case_s1_of2012foamDisplay.UseMipmapTextures = 0
benchmark_case_s1_of2012foamDisplay.BaseColorTexture = None
benchmark_case_s1_of2012foamDisplay.NormalTexture = None
benchmark_case_s1_of2012foamDisplay.NormalScale = 1.0
benchmark_case_s1_of2012foamDisplay.MaterialTexture = None
benchmark_case_s1_of2012foamDisplay.OcclusionStrength = 1.0
benchmark_case_s1_of2012foamDisplay.EmissiveTexture = None
benchmark_case_s1_of2012foamDisplay.EmissiveFactor = [1.0, 1.0, 1.0]
benchmark_case_s1_of2012foamDisplay.FlipTextures = 0
benchmark_case_s1_of2012foamDisplay.BackfaceRepresentation = 'Follow Frontface'
benchmark_case_s1_of2012foamDisplay.BackfaceAmbientColor = [1.0, 1.0, 1.0]
benchmark_case_s1_of2012foamDisplay.BackfaceOpacity = 1.0
benchmark_case_s1_of2012foamDisplay.Position = [0.0, 0.0, 0.0]
benchmark_case_s1_of2012foamDisplay.Scale = [1.0, 1.0, 1.0]
benchmark_case_s1_of2012foamDisplay.Orientation = [0.0, 0.0, 0.0]
benchmark_case_s1_of2012foamDisplay.Origin = [0.0, 0.0, 0.0]
benchmark_case_s1_of2012foamDisplay.Pickable = 1
benchmark_case_s1_of2012foamDisplay.Triangulate = 0
benchmark_case_s1_of2012foamDisplay.UseShaderReplacements = 0
benchmark_case_s1_of2012foamDisplay.ShaderReplacements = ''
benchmark_case_s1_of2012foamDisplay.NonlinearSubdivisionLevel = 1
benchmark_case_s1_of2012foamDisplay.UseDataPartitions = 0
benchmark_case_s1_of2012foamDisplay.OSPRayUseScaleArray = 0
benchmark_case_s1_of2012foamDisplay.OSPRayScaleArray = 'p'
benchmark_case_s1_of2012foamDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
benchmark_case_s1_of2012foamDisplay.OSPRayMaterial = 'None'
benchmark_case_s1_of2012foamDisplay.Orient = 0
benchmark_case_s1_of2012foamDisplay.OrientationMode = 'Direction'
benchmark_case_s1_of2012foamDisplay.SelectOrientationVectors = 'U'
benchmark_case_s1_of2012foamDisplay.Scaling = 0
benchmark_case_s1_of2012foamDisplay.ScaleMode = 'No Data Scaling Off'
benchmark_case_s1_of2012foamDisplay.ScaleFactor = 0.5192030906677246
benchmark_case_s1_of2012foamDisplay.SelectScaleArray = 'p'
benchmark_case_s1_of2012foamDisplay.GlyphType = 'Arrow'
benchmark_case_s1_of2012foamDisplay.UseGlyphTable = 0
benchmark_case_s1_of2012foamDisplay.GlyphTableIndexArray = 'p'
benchmark_case_s1_of2012foamDisplay.UseCompositeGlyphTable = 0
benchmark_case_s1_of2012foamDisplay.UseGlyphCullingAndLOD = 0
benchmark_case_s1_of2012foamDisplay.LODValues = []
benchmark_case_s1_of2012foamDisplay.ColorByLODIndex = 0
benchmark_case_s1_of2012foamDisplay.GaussianRadius = 0.02596015453338623
benchmark_case_s1_of2012foamDisplay.ShaderPreset = 'Sphere'
benchmark_case_s1_of2012foamDisplay.CustomTriangleScale = 3
benchmark_case_s1_of2012foamDisplay.CustomShader = """ // This custom shader code define a gaussian blur
 // Please take a look into vtkSMPointGaussianRepresentation.cxx
 // for other custom shader examples
 //VTK::Color::Impl
   float dist2 = dot(offsetVCVSOutput.xy,offsetVCVSOutput.xy);
   float gaussian = exp(-0.5*dist2);
   opacity = opacity*gaussian;
"""
benchmark_case_s1_of2012foamDisplay.Emissive = 0
benchmark_case_s1_of2012foamDisplay.ScaleByArray = 0
benchmark_case_s1_of2012foamDisplay.SetScaleArray = ['POINTS', 'p']
benchmark_case_s1_of2012foamDisplay.ScaleArrayComponent = ''
benchmark_case_s1_of2012foamDisplay.UseScaleFunction = 1
benchmark_case_s1_of2012foamDisplay.ScaleTransferFunction = 'PiecewiseFunction'
benchmark_case_s1_of2012foamDisplay.OpacityByArray = 0
benchmark_case_s1_of2012foamDisplay.OpacityArray = ['POINTS', 'p']
benchmark_case_s1_of2012foamDisplay.OpacityArrayComponent = ''
benchmark_case_s1_of2012foamDisplay.OpacityTransferFunction = 'PiecewiseFunction'
benchmark_case_s1_of2012foamDisplay.DataAxesGrid = 'GridAxesRepresentation'
benchmark_case_s1_of2012foamDisplay.SelectionCellLabelBold = 0
benchmark_case_s1_of2012foamDisplay.SelectionCellLabelColor = [0.0, 1.0, 0.0]
benchmark_case_s1_of2012foamDisplay.SelectionCellLabelFontFamily = 'Arial'
benchmark_case_s1_of2012foamDisplay.SelectionCellLabelFontFile = ''
benchmark_case_s1_of2012foamDisplay.SelectionCellLabelFontSize = 18
benchmark_case_s1_of2012foamDisplay.SelectionCellLabelItalic = 0
benchmark_case_s1_of2012foamDisplay.SelectionCellLabelJustification = 'Left'
benchmark_case_s1_of2012foamDisplay.SelectionCellLabelOpacity = 1.0
benchmark_case_s1_of2012foamDisplay.SelectionCellLabelShadow = 0
benchmark_case_s1_of2012foamDisplay.SelectionPointLabelBold = 0
benchmark_case_s1_of2012foamDisplay.SelectionPointLabelColor = [1.0, 1.0, 0.0]
benchmark_case_s1_of2012foamDisplay.SelectionPointLabelFontFamily = 'Arial'
benchmark_case_s1_of2012foamDisplay.SelectionPointLabelFontFile = ''
benchmark_case_s1_of2012foamDisplay.SelectionPointLabelFontSize = 18
benchmark_case_s1_of2012foamDisplay.SelectionPointLabelItalic = 0
benchmark_case_s1_of2012foamDisplay.SelectionPointLabelJustification = 'Left'
benchmark_case_s1_of2012foamDisplay.SelectionPointLabelOpacity = 1.0
benchmark_case_s1_of2012foamDisplay.SelectionPointLabelShadow = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes = 'PolarAxesRepresentation'
benchmark_case_s1_of2012foamDisplay.ScalarOpacityFunction = pPWF
benchmark_case_s1_of2012foamDisplay.ScalarOpacityUnitDistance = 0.159739802676222
benchmark_case_s1_of2012foamDisplay.ExtractedBlockIndex = 1
benchmark_case_s1_of2012foamDisplay.SelectMapper = 'Projected tetra'
benchmark_case_s1_of2012foamDisplay.SamplingDimensions = [128, 128, 128]
benchmark_case_s1_of2012foamDisplay.UseFloatingPointFrameBuffer = 1

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
benchmark_case_s1_of2012foamDisplay.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]
benchmark_case_s1_of2012foamDisplay.OSPRayScaleFunction.UseLogScale = 0

# init the 'Arrow' selected for 'GlyphType'
benchmark_case_s1_of2012foamDisplay.GlyphType.TipResolution = 6
benchmark_case_s1_of2012foamDisplay.GlyphType.TipRadius = 0.1
benchmark_case_s1_of2012foamDisplay.GlyphType.TipLength = 0.35
benchmark_case_s1_of2012foamDisplay.GlyphType.ShaftResolution = 6
benchmark_case_s1_of2012foamDisplay.GlyphType.ShaftRadius = 0.03
benchmark_case_s1_of2012foamDisplay.GlyphType.Invert = 0

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
benchmark_case_s1_of2012foamDisplay.ScaleTransferFunction.Points = [-7.349666595458984, 0.0, 0.5, 0.0, 2.4913246631622314, 1.0, 0.5, 0.0]
benchmark_case_s1_of2012foamDisplay.ScaleTransferFunction.UseLogScale = 0

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
benchmark_case_s1_of2012foamDisplay.OpacityTransferFunction.Points = [-7.349666595458984, 0.0, 0.5, 0.0, 2.4913246631622314, 1.0, 0.5, 0.0]
benchmark_case_s1_of2012foamDisplay.OpacityTransferFunction.UseLogScale = 0

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.XTitle = 'X Axis'
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.YTitle = 'Y Axis'
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ZTitle = 'Z Axis'
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.XTitleFontFamily = 'Arial'
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.XTitleFontFile = ''
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.XTitleBold = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.XTitleItalic = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.XTitleFontSize = 12
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.XTitleShadow = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.XTitleOpacity = 1.0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.YTitleFontFamily = 'Arial'
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.YTitleFontFile = ''
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.YTitleBold = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.YTitleItalic = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.YTitleFontSize = 12
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.YTitleShadow = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.YTitleOpacity = 1.0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ZTitleFontFamily = 'Arial'
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ZTitleFontFile = ''
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ZTitleBold = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ZTitleItalic = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ZTitleFontSize = 12
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ZTitleShadow = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ZTitleOpacity = 1.0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.FacesToRender = 63
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.CullBackface = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.CullFrontface = 1
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ShowGrid = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ShowEdges = 1
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ShowTicks = 1
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.LabelUniqueEdgesOnly = 1
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.AxesToLabel = 63
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.XLabelFontFamily = 'Arial'
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.XLabelFontFile = ''
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.XLabelBold = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.XLabelItalic = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.XLabelFontSize = 12
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.XLabelShadow = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.XLabelOpacity = 1.0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.YLabelFontFamily = 'Arial'
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.YLabelFontFile = ''
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.YLabelBold = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.YLabelItalic = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.YLabelFontSize = 12
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.YLabelShadow = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.YLabelOpacity = 1.0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ZLabelFontFamily = 'Arial'
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ZLabelFontFile = ''
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ZLabelBold = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ZLabelItalic = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ZLabelFontSize = 12
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ZLabelShadow = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ZLabelOpacity = 1.0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.XAxisNotation = 'Mixed'
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.XAxisPrecision = 2
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.XAxisUseCustomLabels = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.XAxisLabels = []
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.YAxisNotation = 'Mixed'
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.YAxisPrecision = 2
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.YAxisUseCustomLabels = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.YAxisLabels = []
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ZAxisNotation = 'Mixed'
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ZAxisPrecision = 2
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ZAxisUseCustomLabels = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.ZAxisLabels = []
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.UseCustomBounds = 0
benchmark_case_s1_of2012foamDisplay.DataAxesGrid.CustomBounds = [0.0, 1.0, 0.0, 1.0, 0.0, 1.0]

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
benchmark_case_s1_of2012foamDisplay.PolarAxes.Visibility = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes.Translation = [0.0, 0.0, 0.0]
benchmark_case_s1_of2012foamDisplay.PolarAxes.Scale = [1.0, 1.0, 1.0]
benchmark_case_s1_of2012foamDisplay.PolarAxes.Orientation = [0.0, 0.0, 0.0]
benchmark_case_s1_of2012foamDisplay.PolarAxes.EnableCustomBounds = [0, 0, 0]
benchmark_case_s1_of2012foamDisplay.PolarAxes.CustomBounds = [0.0, 1.0, 0.0, 1.0, 0.0, 1.0]
benchmark_case_s1_of2012foamDisplay.PolarAxes.EnableCustomRange = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes.CustomRange = [0.0, 1.0]
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisVisibility = 1
benchmark_case_s1_of2012foamDisplay.PolarAxes.RadialAxesVisibility = 1
benchmark_case_s1_of2012foamDisplay.PolarAxes.DrawRadialGridlines = 1
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarArcsVisibility = 1
benchmark_case_s1_of2012foamDisplay.PolarAxes.DrawPolarArcsGridlines = 1
benchmark_case_s1_of2012foamDisplay.PolarAxes.NumberOfRadialAxes = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes.AutoSubdividePolarAxis = 1
benchmark_case_s1_of2012foamDisplay.PolarAxes.NumberOfPolarAxis = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes.MinimumRadius = 0.0
benchmark_case_s1_of2012foamDisplay.PolarAxes.MinimumAngle = 0.0
benchmark_case_s1_of2012foamDisplay.PolarAxes.MaximumAngle = 90.0
benchmark_case_s1_of2012foamDisplay.PolarAxes.RadialAxesOriginToPolarAxis = 1
benchmark_case_s1_of2012foamDisplay.PolarAxes.Ratio = 1.0
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisColor = [1.0, 1.0, 1.0]
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarArcsColor = [1.0, 1.0, 1.0]
benchmark_case_s1_of2012foamDisplay.PolarAxes.LastRadialAxisColor = [1.0, 1.0, 1.0]
benchmark_case_s1_of2012foamDisplay.PolarAxes.SecondaryPolarArcsColor = [1.0, 1.0, 1.0]
benchmark_case_s1_of2012foamDisplay.PolarAxes.SecondaryRadialAxesColor = [1.0, 1.0, 1.0]
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisTitleVisibility = 1
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisTitle = 'Radial Distance'
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisTitleLocation = 'Bottom'
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarLabelVisibility = 1
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarLabelFormat = '%-#6.3g'
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarLabelExponentLocation = 'Labels'
benchmark_case_s1_of2012foamDisplay.PolarAxes.RadialLabelVisibility = 1
benchmark_case_s1_of2012foamDisplay.PolarAxes.RadialLabelFormat = '%-#3.1f'
benchmark_case_s1_of2012foamDisplay.PolarAxes.RadialLabelLocation = 'Bottom'
benchmark_case_s1_of2012foamDisplay.PolarAxes.RadialUnitsVisibility = 1
benchmark_case_s1_of2012foamDisplay.PolarAxes.ScreenSize = 10.0
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisTitleOpacity = 1.0
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisTitleFontFamily = 'Arial'
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisTitleFontFile = ''
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisTitleBold = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisTitleItalic = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisTitleShadow = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisTitleFontSize = 12
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisLabelOpacity = 1.0
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisLabelFontFamily = 'Arial'
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisLabelFontFile = ''
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisLabelBold = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisLabelItalic = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisLabelShadow = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisLabelFontSize = 12
benchmark_case_s1_of2012foamDisplay.PolarAxes.LastRadialAxisTextOpacity = 1.0
benchmark_case_s1_of2012foamDisplay.PolarAxes.LastRadialAxisTextFontFamily = 'Arial'
benchmark_case_s1_of2012foamDisplay.PolarAxes.LastRadialAxisTextFontFile = ''
benchmark_case_s1_of2012foamDisplay.PolarAxes.LastRadialAxisTextBold = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes.LastRadialAxisTextItalic = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes.LastRadialAxisTextShadow = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes.LastRadialAxisTextFontSize = 12
benchmark_case_s1_of2012foamDisplay.PolarAxes.SecondaryRadialAxesTextOpacity = 1.0
benchmark_case_s1_of2012foamDisplay.PolarAxes.SecondaryRadialAxesTextFontFamily = 'Arial'
benchmark_case_s1_of2012foamDisplay.PolarAxes.SecondaryRadialAxesTextFontFile = ''
benchmark_case_s1_of2012foamDisplay.PolarAxes.SecondaryRadialAxesTextBold = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes.SecondaryRadialAxesTextItalic = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes.SecondaryRadialAxesTextShadow = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes.SecondaryRadialAxesTextFontSize = 12
benchmark_case_s1_of2012foamDisplay.PolarAxes.EnableDistanceLOD = 1
benchmark_case_s1_of2012foamDisplay.PolarAxes.DistanceLODThreshold = 0.7
benchmark_case_s1_of2012foamDisplay.PolarAxes.EnableViewAngleLOD = 1
benchmark_case_s1_of2012foamDisplay.PolarAxes.ViewAngleLODThreshold = 0.7
benchmark_case_s1_of2012foamDisplay.PolarAxes.SmallestVisiblePolarAngle = 0.5
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarTicksVisibility = 1
benchmark_case_s1_of2012foamDisplay.PolarAxes.ArcTicksOriginToPolarAxis = 1
benchmark_case_s1_of2012foamDisplay.PolarAxes.TickLocation = 'Both'
benchmark_case_s1_of2012foamDisplay.PolarAxes.AxisTickVisibility = 1
benchmark_case_s1_of2012foamDisplay.PolarAxes.AxisMinorTickVisibility = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes.ArcTickVisibility = 1
benchmark_case_s1_of2012foamDisplay.PolarAxes.ArcMinorTickVisibility = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes.DeltaAngleMajor = 10.0
benchmark_case_s1_of2012foamDisplay.PolarAxes.DeltaAngleMinor = 5.0
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisMajorTickSize = 0.0
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisTickRatioSize = 0.3
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisMajorTickThickness = 1.0
benchmark_case_s1_of2012foamDisplay.PolarAxes.PolarAxisTickRatioThickness = 0.5
benchmark_case_s1_of2012foamDisplay.PolarAxes.LastRadialAxisMajorTickSize = 0.0
benchmark_case_s1_of2012foamDisplay.PolarAxes.LastRadialAxisTickRatioSize = 0.3
benchmark_case_s1_of2012foamDisplay.PolarAxes.LastRadialAxisMajorTickThickness = 1.0
benchmark_case_s1_of2012foamDisplay.PolarAxes.LastRadialAxisTickRatioThickness = 0.5
benchmark_case_s1_of2012foamDisplay.PolarAxes.ArcMajorTickSize = 0.0
benchmark_case_s1_of2012foamDisplay.PolarAxes.ArcTickRatioSize = 0.3
benchmark_case_s1_of2012foamDisplay.PolarAxes.ArcMajorTickThickness = 1.0
benchmark_case_s1_of2012foamDisplay.PolarAxes.ArcTickRatioThickness = 0.5
benchmark_case_s1_of2012foamDisplay.PolarAxes.Use2DMode = 0
benchmark_case_s1_of2012foamDisplay.PolarAxes.UseLogAxis = 0

# reset view to fit data
renderView1.ResetCamera()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# show color bar/color legend
benchmark_case_s1_of2012foamDisplay.SetScalarBarVisibility(renderView1, True)

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# Rescale transfer function
pLUT.RescaleTransferFunction(-7.349666595458984, 2.498786449432373)

# Rescale transfer function
pPWF.RescaleTransferFunction(-7.349666595458984, 2.498786449432373)

# create a new 'Slice'
slice1 = Slice(Input=benchmark_case_s1_of2012foam)
slice1.SliceType = 'Plane'
slice1.HyperTreeGridSlicer = 'Plane'
slice1.UseDual = 0
slice1.Crinkleslice = 0
slice1.Triangulatetheslice = 1
slice1.Mergeduplicatedpointsintheslice = 1
slice1.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice1.SliceType.Origin = [-0.40398454666137695, -0.40404725074768066, -1.1920928955078125e-07]
slice1.SliceType.Normal = [1.0, 0.0, 0.0]
slice1.SliceType.Offset = 0.0

# init the 'Plane' selected for 'HyperTreeGridSlicer'
slice1.HyperTreeGridSlicer.Origin = [-0.40398454666137695, -0.40404725074768066, -1.1920928955078125e-07]
slice1.HyperTreeGridSlicer.Normal = [1.0, 0.0, 0.0]
slice1.HyperTreeGridSlicer.Offset = 0.0

# Properties modified on slice1.SliceType
slice1.SliceType.Origin = [-2.995, 0.0, 0.0]

# show data in view
slice1Display = Show(slice1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
slice1Display.Representation = 'Surface'
slice1Display.ColorArrayName = ['POINTS', 'p']
slice1Display.LookupTable = pLUT
slice1Display.MapScalars = 1
slice1Display.MultiComponentsMapping = 0
slice1Display.InterpolateScalarsBeforeMapping = 1
slice1Display.Opacity = 1.0
slice1Display.PointSize = 2.0
slice1Display.LineWidth = 1.0
slice1Display.RenderLinesAsTubes = 0
slice1Display.RenderPointsAsSpheres = 0
slice1Display.Interpolation = 'Gouraud'
slice1Display.Specular = 0.0
slice1Display.SpecularColor = [1.0, 1.0, 1.0]
slice1Display.SpecularPower = 100.0
slice1Display.Luminosity = 0.0
slice1Display.Ambient = 0.0
slice1Display.Diffuse = 1.0
slice1Display.Roughness = 0.3
slice1Display.Metallic = 0.0
slice1Display.Texture = None
slice1Display.RepeatTextures = 1
slice1Display.InterpolateTextures = 0
slice1Display.SeamlessU = 0
slice1Display.SeamlessV = 0
slice1Display.UseMipmapTextures = 0
slice1Display.BaseColorTexture = None
slice1Display.NormalTexture = None
slice1Display.NormalScale = 1.0
slice1Display.MaterialTexture = None
slice1Display.OcclusionStrength = 1.0
slice1Display.EmissiveTexture = None
slice1Display.EmissiveFactor = [1.0, 1.0, 1.0]
slice1Display.FlipTextures = 0
slice1Display.BackfaceRepresentation = 'Follow Frontface'
slice1Display.BackfaceAmbientColor = [1.0, 1.0, 1.0]
slice1Display.BackfaceOpacity = 1.0
slice1Display.Position = [0.0, 0.0, 0.0]
slice1Display.Scale = [1.0, 1.0, 1.0]
slice1Display.Orientation = [0.0, 0.0, 0.0]
slice1Display.Origin = [0.0, 0.0, 0.0]
slice1Display.Pickable = 1
slice1Display.Triangulate = 0
slice1Display.UseShaderReplacements = 0
slice1Display.ShaderReplacements = ''
slice1Display.NonlinearSubdivisionLevel = 1
slice1Display.UseDataPartitions = 0
slice1Display.OSPRayUseScaleArray = 0
slice1Display.OSPRayScaleArray = 'p'
slice1Display.OSPRayScaleFunction = 'PiecewiseFunction'
slice1Display.OSPRayMaterial = 'None'
slice1Display.Orient = 0
slice1Display.OrientationMode = 'Direction'
slice1Display.SelectOrientationVectors = 'U'
slice1Display.Scaling = 0
slice1Display.ScaleMode = 'No Data Scaling Off'
slice1Display.ScaleFactor = 0.09998312890529633
slice1Display.SelectScaleArray = 'p'
slice1Display.GlyphType = 'Arrow'
slice1Display.UseGlyphTable = 0
slice1Display.GlyphTableIndexArray = 'p'
slice1Display.UseCompositeGlyphTable = 0
slice1Display.UseGlyphCullingAndLOD = 0
slice1Display.LODValues = []
slice1Display.ColorByLODIndex = 0
slice1Display.GaussianRadius = 0.004999156445264817
slice1Display.ShaderPreset = 'Sphere'
slice1Display.CustomTriangleScale = 3
slice1Display.CustomShader = """ // This custom shader code define a gaussian blur
 // Please take a look into vtkSMPointGaussianRepresentation.cxx
 // for other custom shader examples
 //VTK::Color::Impl
   float dist2 = dot(offsetVCVSOutput.xy,offsetVCVSOutput.xy);
   float gaussian = exp(-0.5*dist2);
   opacity = opacity*gaussian;
"""
slice1Display.Emissive = 0
slice1Display.ScaleByArray = 0
slice1Display.SetScaleArray = ['POINTS', 'p']
slice1Display.ScaleArrayComponent = ''
slice1Display.UseScaleFunction = 1
slice1Display.ScaleTransferFunction = 'PiecewiseFunction'
slice1Display.OpacityByArray = 0
slice1Display.OpacityArray = ['POINTS', 'p']
slice1Display.OpacityArrayComponent = ''
slice1Display.OpacityTransferFunction = 'PiecewiseFunction'
slice1Display.DataAxesGrid = 'GridAxesRepresentation'
slice1Display.SelectionCellLabelBold = 0
slice1Display.SelectionCellLabelColor = [0.0, 1.0, 0.0]
slice1Display.SelectionCellLabelFontFamily = 'Arial'
slice1Display.SelectionCellLabelFontFile = ''
slice1Display.SelectionCellLabelFontSize = 18
slice1Display.SelectionCellLabelItalic = 0
slice1Display.SelectionCellLabelJustification = 'Left'
slice1Display.SelectionCellLabelOpacity = 1.0
slice1Display.SelectionCellLabelShadow = 0
slice1Display.SelectionPointLabelBold = 0
slice1Display.SelectionPointLabelColor = [1.0, 1.0, 0.0]
slice1Display.SelectionPointLabelFontFamily = 'Arial'
slice1Display.SelectionPointLabelFontFile = ''
slice1Display.SelectionPointLabelFontSize = 18
slice1Display.SelectionPointLabelItalic = 0
slice1Display.SelectionPointLabelJustification = 'Left'
slice1Display.SelectionPointLabelOpacity = 1.0
slice1Display.SelectionPointLabelShadow = 0
slice1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
slice1Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]
slice1Display.OSPRayScaleFunction.UseLogScale = 0

# init the 'Arrow' selected for 'GlyphType'
slice1Display.GlyphType.TipResolution = 6
slice1Display.GlyphType.TipRadius = 0.1
slice1Display.GlyphType.TipLength = 0.35
slice1Display.GlyphType.ShaftResolution = 6
slice1Display.GlyphType.ShaftRadius = 0.03
slice1Display.GlyphType.Invert = 0

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
slice1Display.ScaleTransferFunction.Points = [-0.00026925429119728506, 0.0, 0.5, 0.0, 0.03909657523036003, 1.0, 0.5, 0.0]
slice1Display.ScaleTransferFunction.UseLogScale = 0

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
slice1Display.OpacityTransferFunction.Points = [-0.00026925429119728506, 0.0, 0.5, 0.0, 0.03909657523036003, 1.0, 0.5, 0.0]
slice1Display.OpacityTransferFunction.UseLogScale = 0

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
slice1Display.DataAxesGrid.XTitle = 'X Axis'
slice1Display.DataAxesGrid.YTitle = 'Y Axis'
slice1Display.DataAxesGrid.ZTitle = 'Z Axis'
slice1Display.DataAxesGrid.XTitleFontFamily = 'Arial'
slice1Display.DataAxesGrid.XTitleFontFile = ''
slice1Display.DataAxesGrid.XTitleBold = 0
slice1Display.DataAxesGrid.XTitleItalic = 0
slice1Display.DataAxesGrid.XTitleFontSize = 12
slice1Display.DataAxesGrid.XTitleShadow = 0
slice1Display.DataAxesGrid.XTitleOpacity = 1.0
slice1Display.DataAxesGrid.YTitleFontFamily = 'Arial'
slice1Display.DataAxesGrid.YTitleFontFile = ''
slice1Display.DataAxesGrid.YTitleBold = 0
slice1Display.DataAxesGrid.YTitleItalic = 0
slice1Display.DataAxesGrid.YTitleFontSize = 12
slice1Display.DataAxesGrid.YTitleShadow = 0
slice1Display.DataAxesGrid.YTitleOpacity = 1.0
slice1Display.DataAxesGrid.ZTitleFontFamily = 'Arial'
slice1Display.DataAxesGrid.ZTitleFontFile = ''
slice1Display.DataAxesGrid.ZTitleBold = 0
slice1Display.DataAxesGrid.ZTitleItalic = 0
slice1Display.DataAxesGrid.ZTitleFontSize = 12
slice1Display.DataAxesGrid.ZTitleShadow = 0
slice1Display.DataAxesGrid.ZTitleOpacity = 1.0
slice1Display.DataAxesGrid.FacesToRender = 63
slice1Display.DataAxesGrid.CullBackface = 0
slice1Display.DataAxesGrid.CullFrontface = 1
slice1Display.DataAxesGrid.ShowGrid = 0
slice1Display.DataAxesGrid.ShowEdges = 1
slice1Display.DataAxesGrid.ShowTicks = 1
slice1Display.DataAxesGrid.LabelUniqueEdgesOnly = 1
slice1Display.DataAxesGrid.AxesToLabel = 63
slice1Display.DataAxesGrid.XLabelFontFamily = 'Arial'
slice1Display.DataAxesGrid.XLabelFontFile = ''
slice1Display.DataAxesGrid.XLabelBold = 0
slice1Display.DataAxesGrid.XLabelItalic = 0
slice1Display.DataAxesGrid.XLabelFontSize = 12
slice1Display.DataAxesGrid.XLabelShadow = 0
slice1Display.DataAxesGrid.XLabelOpacity = 1.0
slice1Display.DataAxesGrid.YLabelFontFamily = 'Arial'
slice1Display.DataAxesGrid.YLabelFontFile = ''
slice1Display.DataAxesGrid.YLabelBold = 0
slice1Display.DataAxesGrid.YLabelItalic = 0
slice1Display.DataAxesGrid.YLabelFontSize = 12
slice1Display.DataAxesGrid.YLabelShadow = 0
slice1Display.DataAxesGrid.YLabelOpacity = 1.0
slice1Display.DataAxesGrid.ZLabelFontFamily = 'Arial'
slice1Display.DataAxesGrid.ZLabelFontFile = ''
slice1Display.DataAxesGrid.ZLabelBold = 0
slice1Display.DataAxesGrid.ZLabelItalic = 0
slice1Display.DataAxesGrid.ZLabelFontSize = 12
slice1Display.DataAxesGrid.ZLabelShadow = 0
slice1Display.DataAxesGrid.ZLabelOpacity = 1.0
slice1Display.DataAxesGrid.XAxisNotation = 'Mixed'
slice1Display.DataAxesGrid.XAxisPrecision = 2
slice1Display.DataAxesGrid.XAxisUseCustomLabels = 0
slice1Display.DataAxesGrid.XAxisLabels = []
slice1Display.DataAxesGrid.YAxisNotation = 'Mixed'
slice1Display.DataAxesGrid.YAxisPrecision = 2
slice1Display.DataAxesGrid.YAxisUseCustomLabels = 0
slice1Display.DataAxesGrid.YAxisLabels = []
slice1Display.DataAxesGrid.ZAxisNotation = 'Mixed'
slice1Display.DataAxesGrid.ZAxisPrecision = 2
slice1Display.DataAxesGrid.ZAxisUseCustomLabels = 0
slice1Display.DataAxesGrid.ZAxisLabels = []
slice1Display.DataAxesGrid.UseCustomBounds = 0
slice1Display.DataAxesGrid.CustomBounds = [0.0, 1.0, 0.0, 1.0, 0.0, 1.0]

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
slice1Display.PolarAxes.Visibility = 0
slice1Display.PolarAxes.Translation = [0.0, 0.0, 0.0]
slice1Display.PolarAxes.Scale = [1.0, 1.0, 1.0]
slice1Display.PolarAxes.Orientation = [0.0, 0.0, 0.0]
slice1Display.PolarAxes.EnableCustomBounds = [0, 0, 0]
slice1Display.PolarAxes.CustomBounds = [0.0, 1.0, 0.0, 1.0, 0.0, 1.0]
slice1Display.PolarAxes.EnableCustomRange = 0
slice1Display.PolarAxes.CustomRange = [0.0, 1.0]
slice1Display.PolarAxes.PolarAxisVisibility = 1
slice1Display.PolarAxes.RadialAxesVisibility = 1
slice1Display.PolarAxes.DrawRadialGridlines = 1
slice1Display.PolarAxes.PolarArcsVisibility = 1
slice1Display.PolarAxes.DrawPolarArcsGridlines = 1
slice1Display.PolarAxes.NumberOfRadialAxes = 0
slice1Display.PolarAxes.AutoSubdividePolarAxis = 1
slice1Display.PolarAxes.NumberOfPolarAxis = 0
slice1Display.PolarAxes.MinimumRadius = 0.0
slice1Display.PolarAxes.MinimumAngle = 0.0
slice1Display.PolarAxes.MaximumAngle = 90.0
slice1Display.PolarAxes.RadialAxesOriginToPolarAxis = 1
slice1Display.PolarAxes.Ratio = 1.0
slice1Display.PolarAxes.PolarAxisColor = [1.0, 1.0, 1.0]
slice1Display.PolarAxes.PolarArcsColor = [1.0, 1.0, 1.0]
slice1Display.PolarAxes.LastRadialAxisColor = [1.0, 1.0, 1.0]
slice1Display.PolarAxes.SecondaryPolarArcsColor = [1.0, 1.0, 1.0]
slice1Display.PolarAxes.SecondaryRadialAxesColor = [1.0, 1.0, 1.0]
slice1Display.PolarAxes.PolarAxisTitleVisibility = 1
slice1Display.PolarAxes.PolarAxisTitle = 'Radial Distance'
slice1Display.PolarAxes.PolarAxisTitleLocation = 'Bottom'
slice1Display.PolarAxes.PolarLabelVisibility = 1
slice1Display.PolarAxes.PolarLabelFormat = '%-#6.3g'
slice1Display.PolarAxes.PolarLabelExponentLocation = 'Labels'
slice1Display.PolarAxes.RadialLabelVisibility = 1
slice1Display.PolarAxes.RadialLabelFormat = '%-#3.1f'
slice1Display.PolarAxes.RadialLabelLocation = 'Bottom'
slice1Display.PolarAxes.RadialUnitsVisibility = 1
slice1Display.PolarAxes.ScreenSize = 10.0
slice1Display.PolarAxes.PolarAxisTitleOpacity = 1.0
slice1Display.PolarAxes.PolarAxisTitleFontFamily = 'Arial'
slice1Display.PolarAxes.PolarAxisTitleFontFile = ''
slice1Display.PolarAxes.PolarAxisTitleBold = 0
slice1Display.PolarAxes.PolarAxisTitleItalic = 0
slice1Display.PolarAxes.PolarAxisTitleShadow = 0
slice1Display.PolarAxes.PolarAxisTitleFontSize = 12
slice1Display.PolarAxes.PolarAxisLabelOpacity = 1.0
slice1Display.PolarAxes.PolarAxisLabelFontFamily = 'Arial'
slice1Display.PolarAxes.PolarAxisLabelFontFile = ''
slice1Display.PolarAxes.PolarAxisLabelBold = 0
slice1Display.PolarAxes.PolarAxisLabelItalic = 0
slice1Display.PolarAxes.PolarAxisLabelShadow = 0
slice1Display.PolarAxes.PolarAxisLabelFontSize = 12
slice1Display.PolarAxes.LastRadialAxisTextOpacity = 1.0
slice1Display.PolarAxes.LastRadialAxisTextFontFamily = 'Arial'
slice1Display.PolarAxes.LastRadialAxisTextFontFile = ''
slice1Display.PolarAxes.LastRadialAxisTextBold = 0
slice1Display.PolarAxes.LastRadialAxisTextItalic = 0
slice1Display.PolarAxes.LastRadialAxisTextShadow = 0
slice1Display.PolarAxes.LastRadialAxisTextFontSize = 12
slice1Display.PolarAxes.SecondaryRadialAxesTextOpacity = 1.0
slice1Display.PolarAxes.SecondaryRadialAxesTextFontFamily = 'Arial'
slice1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''
slice1Display.PolarAxes.SecondaryRadialAxesTextBold = 0
slice1Display.PolarAxes.SecondaryRadialAxesTextItalic = 0
slice1Display.PolarAxes.SecondaryRadialAxesTextShadow = 0
slice1Display.PolarAxes.SecondaryRadialAxesTextFontSize = 12
slice1Display.PolarAxes.EnableDistanceLOD = 1
slice1Display.PolarAxes.DistanceLODThreshold = 0.7
slice1Display.PolarAxes.EnableViewAngleLOD = 1
slice1Display.PolarAxes.ViewAngleLODThreshold = 0.7
slice1Display.PolarAxes.SmallestVisiblePolarAngle = 0.5
slice1Display.PolarAxes.PolarTicksVisibility = 1
slice1Display.PolarAxes.ArcTicksOriginToPolarAxis = 1
slice1Display.PolarAxes.TickLocation = 'Both'
slice1Display.PolarAxes.AxisTickVisibility = 1
slice1Display.PolarAxes.AxisMinorTickVisibility = 0
slice1Display.PolarAxes.ArcTickVisibility = 1
slice1Display.PolarAxes.ArcMinorTickVisibility = 0
slice1Display.PolarAxes.DeltaAngleMajor = 10.0
slice1Display.PolarAxes.DeltaAngleMinor = 5.0
slice1Display.PolarAxes.PolarAxisMajorTickSize = 0.0
slice1Display.PolarAxes.PolarAxisTickRatioSize = 0.3
slice1Display.PolarAxes.PolarAxisMajorTickThickness = 1.0
slice1Display.PolarAxes.PolarAxisTickRatioThickness = 0.5
slice1Display.PolarAxes.LastRadialAxisMajorTickSize = 0.0
slice1Display.PolarAxes.LastRadialAxisTickRatioSize = 0.3
slice1Display.PolarAxes.LastRadialAxisMajorTickThickness = 1.0
slice1Display.PolarAxes.LastRadialAxisTickRatioThickness = 0.5
slice1Display.PolarAxes.ArcMajorTickSize = 0.0
slice1Display.PolarAxes.ArcTickRatioSize = 0.3
slice1Display.PolarAxes.ArcMajorTickThickness = 1.0
slice1Display.PolarAxes.ArcTickRatioThickness = 0.5
slice1Display.PolarAxes.Use2DMode = 0
slice1Display.PolarAxes.UseLogAxis = 0

# hide data in view
Hide(benchmark_case_s1_of2012foam, renderView1)

# show color bar/color legend
slice1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# toggle 3D widget visibility (only when running from the GUI)
Hide3DWidgets(proxy=slice1.SliceType)

# reset view to fit data
renderView1.ResetCamera()

# set scalar coloring
ColorBy(slice1Display, ('POINTS', 's1'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(pLUT, renderView1)

# rescale color and/or opacity maps used to include current data range
slice1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
slice1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 's1'
s1LUT = GetColorTransferFunction('s1')

# get opacity transfer function/opacity map for 's1'
s1PWF = GetOpacityTransferFunction('s1')

animationScene1.GoToLast()

# rescale color and/or opacity maps used to exactly fit the current data range
slice1Display.RescaleTransferFunctionToDataRange(False, True)


# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
s1LUT.ApplyPreset('Rainbow Uniform', True)

# Rescale transfer function
s1LUT.RescaleTransferFunction(0.0, 0.1)

# Rescale transfer function
s1PWF.RescaleTransferFunction(0.0, 0.1)

# get color legend/bar for s1LUT in view renderView1
s1LUTColorBar = GetScalarBar(s1LUT, renderView1)

# change scalar bar placement
s1LUTColorBar.WindowLocation = 'AnyLocation'
s1LUTColorBar.Position = [0.8577666874610106, 0.2897526501766784]
s1LUTColorBar.ScalarBarLength = 0.3300000000000006

# current camera placement for renderView1
renderView1.CameraPosition = [-5.252273617925049, 1.50009024143219, -8.435547351837158e-05]
renderView1.CameraFocalPoint = [-2.994999885559082, 1.50009024143219, -8.435547351837158e-05]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 0.7069127726546146

# save screenshot
SaveScreenshot('./image_tube.png', renderView1, ImageResolution=[1603, 849],
    FontScaling='Scale fonts proportionally',
    OverrideColorPalette='WhiteBackground',
    StereoMode='No change',
    TransparentBackground=0, 
    # PNG options
    CompressionLevel='0')

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.CameraPosition = [-5.252273617925049, 1.50009024143219, -8.435547351837158e-05]
renderView1.CameraFocalPoint = [-2.994999885559082, 1.50009024143219, -8.435547351837158e-05]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 0.7069127726546146

#### uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).
