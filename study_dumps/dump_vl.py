#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.6.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'/data/Joel/mixing-elbow')

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS


geompy = geomBuilder.New()

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
inlet = geompy.MakeVertex(1.5, -3, 0)
elbow_start = geompy.MakeVertex(1.5, 0, 0)
elbow_end = geompy.MakeVertex(0, 1.5, 0)
outlet = geompy.MakeVertex(-3, 1.5, 0)
first_leg = geompy.MakeLineTwoPnt(inlet, elbow_start)
second_leg = geompy.MakeLineTwoPnt(elbow_end, outlet)
Arc_1 = geompy.MakeArcCenter(O, elbow_start, elbow_end,False)
elbow_path = geompy.MakeWire([first_leg, second_leg, Arc_1], 1e-07)
main_section = geompy.MakeDiskPntVecR(inlet, OY, 0.5)
out_section = geompy.MakeDiskPntVecR(outlet, OX, 0.5)
Pipe = geompy.MakePipe(main_section, elbow_path)
[geomObj_1,geomObj_2,geomObj_3,geomObj_4,geomObj_5] = geompy.SubShapeAll(Pipe, geompy.ShapeType["FACE"])
inlet_1 = geompy.CreateGroup(Pipe, geompy.ShapeType["FACE"])
outlet_1 = geompy.CreateGroup(Pipe, geompy.ShapeType["FACE"])
geomObj_6 = geompy.CreateGroup(Pipe, geompy.ShapeType["FACE"])
geomObj_7 = geompy.GetInPlace(Pipe, main_section, True)
[geomObj_8] = geompy.SubShapeAll(geomObj_7, geompy.ShapeType["FACE"])
geomObj_9 = geompy.GetInPlace(Pipe, out_section, True)
[geomObj_10] = geompy.SubShapeAll(geomObj_9, geompy.ShapeType["FACE"])
geompy.UnionList(inlet_1, [geomObj_8])
geompy.UnionList(outlet_1, [geomObj_10])
geompy.UnionList(geomObj_6, [geomObj_1, geomObj_2, geomObj_3, geomObj_4, geomObj_5])
wall1 = geompy.CutListOfGroups([geomObj_6], [inlet_1, outlet_1])
insertion_point = geompy.MakeVertex(1.414213562373095, 1.414213562373095, 0)
inlet2 = geompy.MakeVertex(2.121320343559643, 2.121320343559642, 0)
insert_dir = geompy.MakeVector(inlet2, insertion_point)
mix_section = geompy.MakeDiskPntVecR(inlet2, insert_dir, 0.1)
mix_tube = geompy.MakeCylinder(inlet2, insert_dir, 0.1, 1.4)
[geomObj_11,geomObj_12,geomObj_13] = geompy.SubShapeAll(mix_tube, geompy.ShapeType["FACE"])
inlet2_1 = geompy.CreateGroup(mix_tube, geompy.ShapeType["FACE"])
geomObj_14 = geompy.CreateGroup(mix_tube, geompy.ShapeType["FACE"])
geomObj_15 = geompy.GetInPlace(mix_tube, mix_section, True)
[geomObj_16] = geompy.SubShapeAll(geomObj_15, geompy.ShapeType["FACE"])
geompy.UnionList(inlet2_1, [geomObj_16])
geompy.UnionList(geomObj_14, [geomObj_11, geomObj_12, geomObj_13])
wall2 = geompy.CutListOfGroups([geomObj_14], [inlet2_1])
mixing_elbow = geompy.MakeFuseList([Pipe, mix_tube], True, True)
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( inlet, 'inlet' )
geompy.addToStudy( elbow_start, 'elbow_start' )
geompy.addToStudy( elbow_end, 'elbow_end' )
geompy.addToStudy( outlet, 'outlet' )
geompy.addToStudy( first_leg, 'first_leg' )
geompy.addToStudy( second_leg, 'second_leg' )
geompy.addToStudy( Arc_1, 'Arc_1' )
geompy.addToStudy( elbow_path, 'elbow_path' )
geompy.addToStudy( main_section, 'main_section' )
geompy.addToStudy( out_section, 'out_section' )
geompy.addToStudy( Pipe, 'Pipe' )
geompy.addToStudyInFather( Pipe, inlet_1, 'inlet' )
geompy.addToStudyInFather( Pipe, outlet_1, 'outlet' )
geompy.addToStudyInFather( Pipe, wall1, 'wall1' )
geompy.addToStudy( insertion_point, 'insertion_point' )
geompy.addToStudy( inlet2, 'inlet2' )
geompy.addToStudy( insert_dir, 'insert_dir' )
geompy.addToStudy( mix_section, 'mix_section' )
geompy.addToStudy( mix_tube, 'mix_tube' )
geompy.addToStudyInFather( mix_tube, inlet2_1, 'inlet2' )
geompy.addToStudyInFather( mix_tube, wall2, 'wall2' )
geompy.addToStudy( mixing_elbow, 'mixing_elbow' )
[inlet_2, outlet_2, wall1_1, Pipe_1, inlet2_2, wall2_1, mix_tube_1] = geompy.RestoreGivenSubShapes(mixing_elbow, [inlet_1, outlet_1, wall1, Pipe, inlet2_1, wall2, mix_tube], GEOM.FSM_GetInPlace, False, False)
geompy.ExportSTL(mixing_elbow, "/data/Joel/mixing-elbow/mixing_elbow.stl", True, 0.0001, True)

###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()
#smesh.SetEnablePublish( False ) # Set to False to avoid publish in study if not needed or in some particular situations:
                                 # multiples meshes built in parallel, complex and numerous mesh edition (performance)

#hyp_10.SetMaxSize( 0.16 ) ### not created Object
#hyp_10.SetMinSize( 0.0005 ) ### not created Object
#hyp_10.SetSecondOrder( 0 ) ### not created Object
#hyp_10.SetOptimize( 1 ) ### not created Object
#hyp_10.SetFineness( 5 ) ### not created Object
#hyp_10.SetGrowthRate( 0.3 ) ### not created Object
#hyp_10.SetNbSegPerEdge( 3 ) ### not created Object
#hyp_10.SetNbSegPerRadius( 7 ) ### not created Object
#hyp_10.SetChordalError( -1 ) ### not created Object
#hyp_10.SetChordalErrorEnabled( 0 ) ### not created Object
#hyp_10.SetUseSurfaceCurvature( 1 ) ### not created Object
#hyp_10.SetFuseEdges( 1 ) ### not created Object
#hyp_10.SetQuadAllowed( 0 ) ### not created Object
#hyp_11.SetTotalThickness( 0.025 ) ### not created Object
#hyp_11.SetNumberLayers( 3 ) ### not created Object
#hyp_11.SetStretchFactor( 1.2 ) ### not created Object
#hyp_11.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_11.SetFaces( [], 0 ) ### not created Object
#hyp_12.SetTotalThickness( 0.005 ) ### not created Object
#hyp_12.SetNumberLayers( 3 ) ### not created Object
#hyp_12.SetStretchFactor( 1.2 ) ### not created Object
#hyp_12.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_12.SetFaces( [], 0 ) ### not created Object
#hyp_14.SetMaxSize( 0.16 ) ### not created Object
#hyp_14.SetMinSize( 0.0005 ) ### not created Object
#hyp_14.SetSecondOrder( 0 ) ### not created Object
#hyp_14.SetOptimize( 1 ) ### not created Object
#hyp_14.SetFineness( 5 ) ### not created Object
#hyp_14.SetGrowthRate( 0.3 ) ### not created Object
#hyp_14.SetNbSegPerEdge( 3 ) ### not created Object
#hyp_14.SetNbSegPerRadius( 7 ) ### not created Object
#hyp_14.SetChordalError( -1 ) ### not created Object
#hyp_14.SetChordalErrorEnabled( 0 ) ### not created Object
#hyp_14.SetUseSurfaceCurvature( 1 ) ### not created Object
#hyp_14.SetFuseEdges( 1 ) ### not created Object
#hyp_14.SetQuadAllowed( 0 ) ### not created Object
#hyp_15.SetTotalThickness( 0.025 ) ### not created Object
#hyp_15.SetNumberLayers( 3 ) ### not created Object
#hyp_15.SetStretchFactor( 1.2 ) ### not created Object
#hyp_15.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_15.SetFaces( [], 0 ) ### not created Object
#hyp_16.SetTotalThickness( 0.005 ) ### not created Object
#hyp_16.SetNumberLayers( 3 ) ### not created Object
#hyp_16.SetStretchFactor( 1.2 ) ### not created Object
#hyp_16.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_16.SetFaces( [], 0 ) ### not created Object
#hyp_18.SetMaxSize( 0.16 ) ### not created Object
#hyp_18.SetMinSize( 0.0005 ) ### not created Object
#hyp_18.SetSecondOrder( 0 ) ### not created Object
#hyp_18.SetOptimize( 1 ) ### not created Object
#hyp_18.SetFineness( 5 ) ### not created Object
#hyp_18.SetGrowthRate( 0.3 ) ### not created Object
#hyp_18.SetNbSegPerEdge( 3 ) ### not created Object
#hyp_18.SetNbSegPerRadius( 7 ) ### not created Object
#hyp_18.SetChordalError( -1 ) ### not created Object
#hyp_18.SetChordalErrorEnabled( 0 ) ### not created Object
#hyp_18.SetUseSurfaceCurvature( 1 ) ### not created Object
#hyp_18.SetFuseEdges( 1 ) ### not created Object
#hyp_18.SetQuadAllowed( 0 ) ### not created Object
#hyp_19.SetTotalThickness( 0.025 ) ### not created Object
#hyp_19.SetNumberLayers( 3 ) ### not created Object
#hyp_19.SetStretchFactor( 1.2 ) ### not created Object
#hyp_19.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_21.SetMaxSize( 0.16 ) ### not created Object
#hyp_21.SetMinSize( 0.0005 ) ### not created Object
#hyp_21.SetSecondOrder( 0 ) ### not created Object
#hyp_21.SetOptimize( 1 ) ### not created Object
#hyp_21.SetFineness( 5 ) ### not created Object
#hyp_21.SetGrowthRate( 0.3 ) ### not created Object
#hyp_21.SetNbSegPerEdge( 3 ) ### not created Object
#hyp_21.SetNbSegPerRadius( 7 ) ### not created Object
#hyp_21.SetChordalError( -1 ) ### not created Object
#hyp_21.SetChordalErrorEnabled( 0 ) ### not created Object
#hyp_21.SetUseSurfaceCurvature( 1 ) ### not created Object
#hyp_21.SetFuseEdges( 1 ) ### not created Object
#hyp_21.SetQuadAllowed( 0 ) ### not created Object
#hyp_22.SetTotalThickness( 0.025 ) ### not created Object
#hyp_22.SetNumberLayers( 3 ) ### not created Object
#hyp_22.SetStretchFactor( 1.2 ) ### not created Object
#hyp_22.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_22.SetFaces( [], 0 ) ### not created Object
#hyp_23.SetTotalThickness( 0.005 ) ### not created Object
#hyp_23.SetNumberLayers( 3 ) ### not created Object
#hyp_23.SetStretchFactor( 1.2 ) ### not created Object
#hyp_23.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_23.SetFaces( [], 0 ) ### not created Object
#hyp_25.SetMaxSize( 0.16 ) ### not created Object
#hyp_25.SetMinSize( 0.0005 ) ### not created Object
#hyp_25.SetSecondOrder( 0 ) ### not created Object
#hyp_25.SetOptimize( 1 ) ### not created Object
#hyp_25.SetFineness( 5 ) ### not created Object
#hyp_25.SetGrowthRate( 0.3 ) ### not created Object
#hyp_25.SetNbSegPerEdge( 3 ) ### not created Object
#hyp_25.SetNbSegPerRadius( 7 ) ### not created Object
#hyp_25.SetChordalError( -1 ) ### not created Object
#hyp_25.SetChordalErrorEnabled( 0 ) ### not created Object
#hyp_25.SetUseSurfaceCurvature( 1 ) ### not created Object
#hyp_25.SetFuseEdges( 1 ) ### not created Object
#hyp_25.SetQuadAllowed( 0 ) ### not created Object
#hyp_26.SetTotalThickness( 0.025 ) ### not created Object
#hyp_26.SetNumberLayers( 3 ) ### not created Object
#hyp_26.SetStretchFactor( 1.2 ) ### not created Object
#hyp_26.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_26.SetFaces( [], 0 ) ### not created Object
#hyp_27.SetTotalThickness( 0.005 ) ### not created Object
#hyp_27.SetNumberLayers( 3 ) ### not created Object
#hyp_27.SetStretchFactor( 1.2 ) ### not created Object
#hyp_27.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_27.SetFaces( [], 0 ) ### not created Object
#hyp_29.SetMaxSize( 0.16 ) ### not created Object
#hyp_29.SetMinSize( 0.0005 ) ### not created Object
#hyp_29.SetSecondOrder( 0 ) ### not created Object
#hyp_29.SetOptimize( 1 ) ### not created Object
#hyp_29.SetFineness( 5 ) ### not created Object
#hyp_29.SetGrowthRate( 0.3 ) ### not created Object
#hyp_29.SetNbSegPerEdge( 3 ) ### not created Object
#hyp_29.SetNbSegPerRadius( 7 ) ### not created Object
#hyp_29.SetChordalError( -1 ) ### not created Object
#hyp_29.SetChordalErrorEnabled( 0 ) ### not created Object
#hyp_29.SetUseSurfaceCurvature( 1 ) ### not created Object
#hyp_29.SetFuseEdges( 1 ) ### not created Object
#hyp_29.SetQuadAllowed( 0 ) ### not created Object
#hyp_30.SetTotalThickness( 0.025 ) ### not created Object
#hyp_30.SetNumberLayers( 3 ) ### not created Object
#hyp_30.SetStretchFactor( 1.2 ) ### not created Object
#hyp_30.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_30.SetFaces( [], 0 ) ### not created Object
#hyp_31.SetTotalThickness( 0.005 ) ### not created Object
#hyp_31.SetNumberLayers( 3 ) ### not created Object
#hyp_31.SetStretchFactor( 1.2 ) ### not created Object
#hyp_31.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_31.SetFaces( [], 0 ) ### not created Object
#hyp_33.SetMaxSize( 0.16 ) ### not created Object
#hyp_33.SetMinSize( 0.0005 ) ### not created Object
#hyp_33.SetSecondOrder( 0 ) ### not created Object
#hyp_33.SetOptimize( 1 ) ### not created Object
#hyp_33.SetFineness( 5 ) ### not created Object
#hyp_33.SetGrowthRate( 0.3 ) ### not created Object
#hyp_33.SetNbSegPerEdge( 3 ) ### not created Object
#hyp_33.SetNbSegPerRadius( 7 ) ### not created Object
#hyp_33.SetChordalError( -1 ) ### not created Object
#hyp_33.SetChordalErrorEnabled( 0 ) ### not created Object
#hyp_33.SetUseSurfaceCurvature( 1 ) ### not created Object
#hyp_33.SetFuseEdges( 1 ) ### not created Object
#hyp_33.SetQuadAllowed( 0 ) ### not created Object
#hyp_34.SetTotalThickness( 0.025 ) ### not created Object
#hyp_34.SetNumberLayers( 3 ) ### not created Object
#hyp_34.SetStretchFactor( 1.2 ) ### not created Object
#hyp_34.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_34.SetFaces( [], 0 ) ### not created Object
#hyp_35.SetTotalThickness( 0.005 ) ### not created Object
#hyp_35.SetNumberLayers( 3 ) ### not created Object
#hyp_35.SetStretchFactor( 1.2 ) ### not created Object
#hyp_35.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_35.SetFaces( [], 0 ) ### not created Object
#hyp_37.SetMaxSize( 0.16 ) ### not created Object
#hyp_37.SetMinSize( 0.0005 ) ### not created Object
#hyp_37.SetSecondOrder( 0 ) ### not created Object
#hyp_37.SetOptimize( 1 ) ### not created Object
#hyp_37.SetFineness( 5 ) ### not created Object
#hyp_37.SetGrowthRate( 0.3 ) ### not created Object
#hyp_37.SetNbSegPerEdge( 3 ) ### not created Object
#hyp_37.SetNbSegPerRadius( 7 ) ### not created Object
#hyp_37.SetChordalError( -1 ) ### not created Object
#hyp_37.SetChordalErrorEnabled( 0 ) ### not created Object
#hyp_37.SetUseSurfaceCurvature( 1 ) ### not created Object
#hyp_37.SetFuseEdges( 1 ) ### not created Object
#hyp_37.SetQuadAllowed( 0 ) ### not created Object
#hyp_38.SetTotalThickness( 0.025 ) ### not created Object
#hyp_38.SetNumberLayers( 3 ) ### not created Object
#hyp_38.SetStretchFactor( 1.2 ) ### not created Object
#hyp_38.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_38.SetFaces( [], 0 ) ### not created Object
#hyp_39.SetTotalThickness( 0.005 ) ### not created Object
#hyp_39.SetNumberLayers( 3 ) ### not created Object
#hyp_39.SetStretchFactor( 1.2 ) ### not created Object
#hyp_39.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_39.SetFaces( [], 0 ) ### not created Object
#hyp_41.SetMaxSize( 0.16 ) ### not created Object
#hyp_41.SetMinSize( 0.0005 ) ### not created Object
#hyp_41.SetSecondOrder( 0 ) ### not created Object
#hyp_41.SetOptimize( 1 ) ### not created Object
#hyp_41.SetFineness( 5 ) ### not created Object
#hyp_41.SetGrowthRate( 0.3 ) ### not created Object
#hyp_41.SetNbSegPerEdge( 3 ) ### not created Object
#hyp_41.SetNbSegPerRadius( 7 ) ### not created Object
#hyp_41.SetChordalError( -1 ) ### not created Object
#hyp_41.SetChordalErrorEnabled( 0 ) ### not created Object
#hyp_41.SetUseSurfaceCurvature( 1 ) ### not created Object
#hyp_41.SetFuseEdges( 1 ) ### not created Object
#hyp_41.SetQuadAllowed( 0 ) ### not created Object
#hyp_42.SetTotalThickness( 0.025 ) ### not created Object
#hyp_42.SetNumberLayers( 3 ) ### not created Object
#hyp_42.SetStretchFactor( 1.2 ) ### not created Object
#hyp_42.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_42.SetFaces( [], 0 ) ### not created Object
#hyp_43.SetTotalThickness( 0.005 ) ### not created Object
#hyp_43.SetNumberLayers( 3 ) ### not created Object
#hyp_43.SetStretchFactor( 1.2 ) ### not created Object
#hyp_43.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_43.SetFaces( [], 0 ) ### not created Object
#hyp_45.SetMaxSize( 0.16 ) ### not created Object
#hyp_45.SetMinSize( 0.0005 ) ### not created Object
#hyp_45.SetSecondOrder( 0 ) ### not created Object
#hyp_45.SetOptimize( 1 ) ### not created Object
#hyp_45.SetFineness( 5 ) ### not created Object
#hyp_45.SetGrowthRate( 0.3 ) ### not created Object
#hyp_45.SetNbSegPerEdge( 3 ) ### not created Object
#hyp_45.SetNbSegPerRadius( 7 ) ### not created Object
#hyp_45.SetChordalError( -1 ) ### not created Object
#hyp_45.SetChordalErrorEnabled( 0 ) ### not created Object
#hyp_45.SetUseSurfaceCurvature( 1 ) ### not created Object
#hyp_45.SetFuseEdges( 1 ) ### not created Object
#hyp_45.SetQuadAllowed( 0 ) ### not created Object
#hyp_46.SetTotalThickness( 0.025 ) ### not created Object
#hyp_46.SetNumberLayers( 3 ) ### not created Object
#hyp_46.SetStretchFactor( 1.2 ) ### not created Object
#hyp_46.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_46.SetFaces( [], 0 ) ### not created Object
#hyp_47.SetTotalThickness( 0.005 ) ### not created Object
#hyp_47.SetNumberLayers( 3 ) ### not created Object
#hyp_47.SetStretchFactor( 1.2 ) ### not created Object
#hyp_47.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_47.SetFaces( [], 0 ) ### not created Object
#hyp_49.SetMaxSize( 0.16 ) ### not created Object
#hyp_49.SetMinSize( 0.0005 ) ### not created Object
#hyp_49.SetSecondOrder( 0 ) ### not created Object
#hyp_49.SetOptimize( 1 ) ### not created Object
#hyp_49.SetFineness( 5 ) ### not created Object
#hyp_49.SetGrowthRate( 0.3 ) ### not created Object
#hyp_49.SetNbSegPerEdge( 3 ) ### not created Object
#hyp_49.SetNbSegPerRadius( 7 ) ### not created Object
#hyp_49.SetChordalError( -1 ) ### not created Object
#hyp_49.SetChordalErrorEnabled( 0 ) ### not created Object
#hyp_49.SetUseSurfaceCurvature( 1 ) ### not created Object
#hyp_49.SetFuseEdges( 1 ) ### not created Object
#hyp_49.SetQuadAllowed( 0 ) ### not created Object
#hyp_50.SetTotalThickness( 0.025 ) ### not created Object
#hyp_50.SetNumberLayers( 3 ) ### not created Object
#hyp_50.SetStretchFactor( 1.2 ) ### not created Object
#hyp_50.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_50.SetFaces( [], 0 ) ### not created Object
#hyp_51.SetTotalThickness( 0.005 ) ### not created Object
#hyp_51.SetNumberLayers( 3 ) ### not created Object
#hyp_51.SetStretchFactor( 1.2 ) ### not created Object
#hyp_51.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_51.SetFaces( [], 0 ) ### not created Object
#hyp_53.SetMaxSize( 0.16 ) ### not created Object
#hyp_53.SetMinSize( 0.0005 ) ### not created Object
#hyp_53.SetSecondOrder( 0 ) ### not created Object
#hyp_53.SetOptimize( 1 ) ### not created Object
#hyp_53.SetFineness( 5 ) ### not created Object
#hyp_53.SetGrowthRate( 0.3 ) ### not created Object
#hyp_53.SetNbSegPerEdge( 3 ) ### not created Object
#hyp_53.SetNbSegPerRadius( 7 ) ### not created Object
#hyp_53.SetChordalError( -1 ) ### not created Object
#hyp_53.SetChordalErrorEnabled( 0 ) ### not created Object
#hyp_53.SetUseSurfaceCurvature( 1 ) ### not created Object
#hyp_53.SetFuseEdges( 1 ) ### not created Object
#hyp_53.SetQuadAllowed( 0 ) ### not created Object
#hyp_54.SetTotalThickness( 0.025 ) ### not created Object
#hyp_54.SetNumberLayers( 3 ) ### not created Object
#hyp_54.SetStretchFactor( 1.2 ) ### not created Object
#hyp_54.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_54.SetFaces( [], 0 ) ### not created Object
#hyp_55.SetTotalThickness( 0.005 ) ### not created Object
#hyp_55.SetNumberLayers( 3 ) ### not created Object
#hyp_55.SetStretchFactor( 1.2 ) ### not created Object
#hyp_55.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH ) ### not created Object
#hyp_55.SetFaces( [], 0 ) ### not created Object
mesh_elbow = smesh.Mesh(mixing_elbow)
NETGEN_1D_2D_3D = mesh_elbow.Tetrahedron(algo=smeshBuilder.NETGEN_1D2D3D)
Hp01 = NETGEN_1D_2D_3D.Parameters()
Hp01.SetMaxSize( 0.16 )
Hp01.SetMinSize( 0.0005 )
Hp01.SetSecondOrder( 0 )
Hp01.SetOptimize( 1 )
Hp01.SetFineness( 5 )
Hp01.SetGrowthRate( 0.3 )
Hp01.SetNbSegPerEdge( 3 )
Hp01.SetNbSegPerRadius( 7 )
Hp01.SetChordalError( -1 )
Hp01.SetChordalErrorEnabled( 0 )
Hp01.SetUseSurfaceCurvature( 1 )
Hp01.SetFuseEdges( 1 )
Hp01.SetQuadAllowed( 0 )
ViscousLayers_3 = NETGEN_1D_2D_3D.ViscousLayers(0.005,3,1.2,[],0,smeshBuilder.SURF_OFFSET_SMOOTH)
ViscousLayers_2 = NETGEN_1D_2D_3D.ViscousLayers(0.025,3,1.2,[],0,smeshBuilder.SURF_OFFSET_SMOOTH)
NETGEN_1D_2D_3D_1 = mesh_elbow.Tetrahedron(algo=smeshBuilder.NETGEN_1D2D3D)
try:
  mesh_elbow.ExportUNV( r'/data/Joel/mixing-elbow/mesh_elbow.unv' )
  pass
except:
  print('ExportUNV() failed. Invalid file name?')
inlet_3 = mesh_elbow.GroupOnGeom(inlet_2,'inlet',SMESH.FACE)
outlet_3 = mesh_elbow.GroupOnGeom(outlet_2,'outlet',SMESH.FACE)
wall1_2 = mesh_elbow.GroupOnGeom(wall1_1,'wall1',SMESH.FACE)
inlet2_3 = mesh_elbow.GroupOnGeom(inlet2_2,'inlet2',SMESH.FACE)
wall2_2 = mesh_elbow.GroupOnGeom(wall2_1,'wall2',SMESH.FACE)
isDone = mesh_elbow.Compute()
[ inlet_3, outlet_3, wall1_2, inlet2_3, wall2_2 ] = mesh_elbow.GetGroups()
ViscousLayers_3.SetTotalThickness( 0.005 )
ViscousLayers_3.SetNumberLayers( 3 )
ViscousLayers_3.SetStretchFactor( 1.2 )
ViscousLayers_3.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH )
ViscousLayers_3.SetFaces( [ 25 ], 0 )
ViscousLayers_2.SetTotalThickness( 0.025 )
ViscousLayers_2.SetNumberLayers( 3 )
ViscousLayers_2.SetStretchFactor( 1.2 )
ViscousLayers_2.SetMethod( smeshBuilder.SURF_OFFSET_SMOOTH )
ViscousLayers_2.SetFaces( [ 7, 12, 20 ], 0 )
isDone = mesh_elbow.Compute()
[ inlet_3, outlet_3, wall1_2, inlet2_3, wall2_2 ] = mesh_elbow.GetGroups()


## Set names of Mesh objects
smesh.SetName(NETGEN_1D_2D_3D.GetAlgorithm(), 'NETGEN 1D-2D-3D')
smesh.SetName(wall1_2, 'wall1')
smesh.SetName(inlet2_3, 'inlet2')
smesh.SetName(ViscousLayers_2, 'ViscousLayers_2')
smesh.SetName(ViscousLayers_3, 'ViscousLayers_3')
smesh.SetName(Hp01, 'Hp01')
smesh.SetName(inlet_3, 'inlet')
smesh.SetName(outlet_3, 'outlet')
smesh.SetName(mesh_elbow.GetMesh(), 'mesh_elbow')
smesh.SetName(wall2_2, 'wall2')


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
