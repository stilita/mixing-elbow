# trace generated using paraview version 5.8.0
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

import os

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

dir_path = os.path.dirname(os.path.realpath(__file__))

case_name = dir_path.split('/')[-1]

file_name = dir_path+'/'+case_name+'.foam'

# create a new 'OpenFOAMReader'
# s1 = OpenFOAMReader(FileName='/data/Joel/mixing-elbow/cases/prototype_case/prototype_case.foam')
s1 = OpenFOAMReader(FileName=file_name)
s1.SkipZeroTime = 1
s1.CaseType = 'Reconstructed Case'
s1.LabelSize = '32-bit'
s1.ScalarSize = '64-bit (DP)'
s1.Createcelltopointfiltereddata = 1
s1.Adddimensionalunitstoarraynames = 0
s1.MeshRegions = ['internalMesh']
s1.CellArrays = ['U', 'p', 's1', 'yPlus']
s1.PointArrays = []
s1.LagrangianArrays = []
s1.Cachemesh = 1
s1.Decomposepolyhedra = 1
s1.ListtimestepsaccordingtocontrolDict = 0
s1.Lagrangianpositionswithoutextradata = 1
s1.Readzones = 0
s1.Copydatatocellzones = 0

# get animation scene
animationScene1 = GetAnimationScene()

# get the time-keeper
timeKeeper1 = GetTimeKeeper()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# Properties modified on s1
s1.CaseType = 'Decomposed Case'

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [1603, 849]

# get layout
layout1 = GetLayout()

# show data in view
s1Display = Show(s1, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'p'
pLUT = GetColorTransferFunction('p')

# get opacity transfer function/opacity map for 'p'
pPWF = GetOpacityTransferFunction('p')

# trace defaults for the display properties.
s1Display.Representation = 'Surface'
s1Display.ColorArrayName = ['POINTS', 'p']
s1Display.LookupTable = pLUT
s1Display.MapScalars = 1
s1Display.MultiComponentsMapping = 0
s1Display.InterpolateScalarsBeforeMapping = 1
s1Display.Opacity = 1.0
s1Display.PointSize = 2.0
s1Display.LineWidth = 1.0
s1Display.RenderLinesAsTubes = 0
s1Display.RenderPointsAsSpheres = 0
s1Display.Interpolation = 'Gouraud'
s1Display.Specular = 0.0
s1Display.SpecularColor = [1.0, 1.0, 1.0]
s1Display.SpecularPower = 100.0
s1Display.Luminosity = 0.0
s1Display.Ambient = 0.0
s1Display.Diffuse = 1.0
s1Display.Roughness = 0.3
s1Display.Metallic = 0.0
s1Display.Texture = None
s1Display.RepeatTextures = 1
s1Display.InterpolateTextures = 0
s1Display.SeamlessU = 0
s1Display.SeamlessV = 0
s1Display.UseMipmapTextures = 0
s1Display.BaseColorTexture = None
s1Display.NormalTexture = None
s1Display.NormalScale = 1.0
s1Display.MaterialTexture = None
s1Display.OcclusionStrength = 1.0
s1Display.EmissiveTexture = None
s1Display.EmissiveFactor = [1.0, 1.0, 1.0]
s1Display.FlipTextures = 0
s1Display.BackfaceRepresentation = 'Follow Frontface'
s1Display.BackfaceAmbientColor = [1.0, 1.0, 1.0]
s1Display.BackfaceOpacity = 1.0
s1Display.Position = [0.0, 0.0, 0.0]
s1Display.Scale = [1.0, 1.0, 1.0]
s1Display.Orientation = [0.0, 0.0, 0.0]
s1Display.Origin = [0.0, 0.0, 0.0]
s1Display.Pickable = 1
s1Display.Triangulate = 0
s1Display.UseShaderReplacements = 0
s1Display.ShaderReplacements = ''
s1Display.NonlinearSubdivisionLevel = 1
s1Display.UseDataPartitions = 0
s1Display.OSPRayUseScaleArray = 0
s1Display.OSPRayScaleArray = 'p'
s1Display.OSPRayScaleFunction = 'PiecewiseFunction'
s1Display.OSPRayMaterial = 'None'
s1Display.Orient = 0
s1Display.OrientationMode = 'Direction'
s1Display.SelectOrientationVectors = 'U'
s1Display.Scaling = 0
s1Display.ScaleMode = 'No Data Scaling Off'
s1Display.ScaleFactor = 0.5192030906677246
s1Display.SelectScaleArray = 'p'
s1Display.GlyphType = 'Arrow'
s1Display.UseGlyphTable = 0
s1Display.GlyphTableIndexArray = 'p'
s1Display.UseCompositeGlyphTable = 0
s1Display.UseGlyphCullingAndLOD = 0
s1Display.LODValues = []
s1Display.ColorByLODIndex = 0
s1Display.GaussianRadius = 0.02596015453338623
s1Display.ShaderPreset = 'Sphere'
s1Display.CustomTriangleScale = 3
s1Display.CustomShader = """ // This custom shader code define a gaussian blur
 // Please take a look into vtkSMPointGaussianRepresentation.cxx
 // for other custom shader examples
 //VTK::Color::Impl
   float dist2 = dot(offsetVCVSOutput.xy,offsetVCVSOutput.xy);
   float gaussian = exp(-0.5*dist2);
   opacity = opacity*gaussian;
"""
s1Display.Emissive = 0
s1Display.ScaleByArray = 0
s1Display.SetScaleArray = ['POINTS', 'p']
s1Display.ScaleArrayComponent = ''
s1Display.UseScaleFunction = 1
s1Display.ScaleTransferFunction = 'PiecewiseFunction'
s1Display.OpacityByArray = 0
s1Display.OpacityArray = ['POINTS', 'p']
s1Display.OpacityArrayComponent = ''
s1Display.OpacityTransferFunction = 'PiecewiseFunction'
s1Display.DataAxesGrid = 'GridAxesRepresentation'
s1Display.SelectionCellLabelBold = 0
s1Display.SelectionCellLabelColor = [0.0, 1.0, 0.0]
s1Display.SelectionCellLabelFontFamily = 'Arial'
s1Display.SelectionCellLabelFontFile = ''
s1Display.SelectionCellLabelFontSize = 18
s1Display.SelectionCellLabelItalic = 0
s1Display.SelectionCellLabelJustification = 'Left'
s1Display.SelectionCellLabelOpacity = 1.0
s1Display.SelectionCellLabelShadow = 0
s1Display.SelectionPointLabelBold = 0
s1Display.SelectionPointLabelColor = [1.0, 1.0, 0.0]
s1Display.SelectionPointLabelFontFamily = 'Arial'
s1Display.SelectionPointLabelFontFile = ''
s1Display.SelectionPointLabelFontSize = 18
s1Display.SelectionPointLabelItalic = 0
s1Display.SelectionPointLabelJustification = 'Left'
s1Display.SelectionPointLabelOpacity = 1.0
s1Display.SelectionPointLabelShadow = 0
s1Display.PolarAxes = 'PolarAxesRepresentation'
s1Display.ScalarOpacityFunction = pPWF
s1Display.ScalarOpacityUnitDistance = 0.159739802676222
s1Display.ExtractedBlockIndex = 1
s1Display.SelectMapper = 'Projected tetra'
s1Display.SamplingDimensions = [128, 128, 128]
s1Display.UseFloatingPointFrameBuffer = 1

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
s1Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]
s1Display.OSPRayScaleFunction.UseLogScale = 0

# init the 'Arrow' selected for 'GlyphType'
s1Display.GlyphType.TipResolution = 6
s1Display.GlyphType.TipRadius = 0.1
s1Display.GlyphType.TipLength = 0.35
s1Display.GlyphType.ShaftResolution = 6
s1Display.GlyphType.ShaftRadius = 0.03
s1Display.GlyphType.Invert = 0

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
s1Display.ScaleTransferFunction.Points = [-7.349666595458984, 0.0, 0.5, 0.0, 2.4913246631622314, 1.0, 0.5, 0.0]
s1Display.ScaleTransferFunction.UseLogScale = 0

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
s1Display.OpacityTransferFunction.Points = [-7.349666595458984, 0.0, 0.5, 0.0, 2.4913246631622314, 1.0, 0.5, 0.0]
s1Display.OpacityTransferFunction.UseLogScale = 0

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
s1Display.DataAxesGrid.XTitle = 'X Axis'
s1Display.DataAxesGrid.YTitle = 'Y Axis'
s1Display.DataAxesGrid.ZTitle = 'Z Axis'
s1Display.DataAxesGrid.XTitleFontFamily = 'Arial'
s1Display.DataAxesGrid.XTitleFontFile = ''
s1Display.DataAxesGrid.XTitleBold = 0
s1Display.DataAxesGrid.XTitleItalic = 0
s1Display.DataAxesGrid.XTitleFontSize = 12
s1Display.DataAxesGrid.XTitleShadow = 0
s1Display.DataAxesGrid.XTitleOpacity = 1.0
s1Display.DataAxesGrid.YTitleFontFamily = 'Arial'
s1Display.DataAxesGrid.YTitleFontFile = ''
s1Display.DataAxesGrid.YTitleBold = 0
s1Display.DataAxesGrid.YTitleItalic = 0
s1Display.DataAxesGrid.YTitleFontSize = 12
s1Display.DataAxesGrid.YTitleShadow = 0
s1Display.DataAxesGrid.YTitleOpacity = 1.0
s1Display.DataAxesGrid.ZTitleFontFamily = 'Arial'
s1Display.DataAxesGrid.ZTitleFontFile = ''
s1Display.DataAxesGrid.ZTitleBold = 0
s1Display.DataAxesGrid.ZTitleItalic = 0
s1Display.DataAxesGrid.ZTitleFontSize = 12
s1Display.DataAxesGrid.ZTitleShadow = 0
s1Display.DataAxesGrid.ZTitleOpacity = 1.0
s1Display.DataAxesGrid.FacesToRender = 63
s1Display.DataAxesGrid.CullBackface = 0
s1Display.DataAxesGrid.CullFrontface = 1
s1Display.DataAxesGrid.ShowGrid = 0
s1Display.DataAxesGrid.ShowEdges = 1
s1Display.DataAxesGrid.ShowTicks = 1
s1Display.DataAxesGrid.LabelUniqueEdgesOnly = 1
s1Display.DataAxesGrid.AxesToLabel = 63
s1Display.DataAxesGrid.XLabelFontFamily = 'Arial'
s1Display.DataAxesGrid.XLabelFontFile = ''
s1Display.DataAxesGrid.XLabelBold = 0
s1Display.DataAxesGrid.XLabelItalic = 0
s1Display.DataAxesGrid.XLabelFontSize = 12
s1Display.DataAxesGrid.XLabelShadow = 0
s1Display.DataAxesGrid.XLabelOpacity = 1.0
s1Display.DataAxesGrid.YLabelFontFamily = 'Arial'
s1Display.DataAxesGrid.YLabelFontFile = ''
s1Display.DataAxesGrid.YLabelBold = 0
s1Display.DataAxesGrid.YLabelItalic = 0
s1Display.DataAxesGrid.YLabelFontSize = 12
s1Display.DataAxesGrid.YLabelShadow = 0
s1Display.DataAxesGrid.YLabelOpacity = 1.0
s1Display.DataAxesGrid.ZLabelFontFamily = 'Arial'
s1Display.DataAxesGrid.ZLabelFontFile = ''
s1Display.DataAxesGrid.ZLabelBold = 0
s1Display.DataAxesGrid.ZLabelItalic = 0
s1Display.DataAxesGrid.ZLabelFontSize = 12
s1Display.DataAxesGrid.ZLabelShadow = 0
s1Display.DataAxesGrid.ZLabelOpacity = 1.0
s1Display.DataAxesGrid.XAxisNotation = 'Mixed'
s1Display.DataAxesGrid.XAxisPrecision = 2
s1Display.DataAxesGrid.XAxisUseCustomLabels = 0
s1Display.DataAxesGrid.XAxisLabels = []
s1Display.DataAxesGrid.YAxisNotation = 'Mixed'
s1Display.DataAxesGrid.YAxisPrecision = 2
s1Display.DataAxesGrid.YAxisUseCustomLabels = 0
s1Display.DataAxesGrid.YAxisLabels = []
s1Display.DataAxesGrid.ZAxisNotation = 'Mixed'
s1Display.DataAxesGrid.ZAxisPrecision = 2
s1Display.DataAxesGrid.ZAxisUseCustomLabels = 0
s1Display.DataAxesGrid.ZAxisLabels = []
s1Display.DataAxesGrid.UseCustomBounds = 0
s1Display.DataAxesGrid.CustomBounds = [0.0, 1.0, 0.0, 1.0, 0.0, 1.0]

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
s1Display.PolarAxes.Visibility = 0
s1Display.PolarAxes.Translation = [0.0, 0.0, 0.0]
s1Display.PolarAxes.Scale = [1.0, 1.0, 1.0]
s1Display.PolarAxes.Orientation = [0.0, 0.0, 0.0]
s1Display.PolarAxes.EnableCustomBounds = [0, 0, 0]
s1Display.PolarAxes.CustomBounds = [0.0, 1.0, 0.0, 1.0, 0.0, 1.0]
s1Display.PolarAxes.EnableCustomRange = 0
s1Display.PolarAxes.CustomRange = [0.0, 1.0]
s1Display.PolarAxes.PolarAxisVisibility = 1
s1Display.PolarAxes.RadialAxesVisibility = 1
s1Display.PolarAxes.DrawRadialGridlines = 1
s1Display.PolarAxes.PolarArcsVisibility = 1
s1Display.PolarAxes.DrawPolarArcsGridlines = 1
s1Display.PolarAxes.NumberOfRadialAxes = 0
s1Display.PolarAxes.AutoSubdividePolarAxis = 1
s1Display.PolarAxes.NumberOfPolarAxis = 0
s1Display.PolarAxes.MinimumRadius = 0.0
s1Display.PolarAxes.MinimumAngle = 0.0
s1Display.PolarAxes.MaximumAngle = 90.0
s1Display.PolarAxes.RadialAxesOriginToPolarAxis = 1
s1Display.PolarAxes.Ratio = 1.0
s1Display.PolarAxes.PolarAxisColor = [1.0, 1.0, 1.0]
s1Display.PolarAxes.PolarArcsColor = [1.0, 1.0, 1.0]
s1Display.PolarAxes.LastRadialAxisColor = [1.0, 1.0, 1.0]
s1Display.PolarAxes.SecondaryPolarArcsColor = [1.0, 1.0, 1.0]
s1Display.PolarAxes.SecondaryRadialAxesColor = [1.0, 1.0, 1.0]
s1Display.PolarAxes.PolarAxisTitleVisibility = 1
s1Display.PolarAxes.PolarAxisTitle = 'Radial Distance'
s1Display.PolarAxes.PolarAxisTitleLocation = 'Bottom'
s1Display.PolarAxes.PolarLabelVisibility = 1
s1Display.PolarAxes.PolarLabelFormat = '%-#6.3g'
s1Display.PolarAxes.PolarLabelExponentLocation = 'Labels'
s1Display.PolarAxes.RadialLabelVisibility = 1
s1Display.PolarAxes.RadialLabelFormat = '%-#3.1f'
s1Display.PolarAxes.RadialLabelLocation = 'Bottom'
s1Display.PolarAxes.RadialUnitsVisibility = 1
s1Display.PolarAxes.ScreenSize = 10.0
s1Display.PolarAxes.PolarAxisTitleOpacity = 1.0
s1Display.PolarAxes.PolarAxisTitleFontFamily = 'Arial'
s1Display.PolarAxes.PolarAxisTitleFontFile = ''
s1Display.PolarAxes.PolarAxisTitleBold = 0
s1Display.PolarAxes.PolarAxisTitleItalic = 0
s1Display.PolarAxes.PolarAxisTitleShadow = 0
s1Display.PolarAxes.PolarAxisTitleFontSize = 12
s1Display.PolarAxes.PolarAxisLabelOpacity = 1.0
s1Display.PolarAxes.PolarAxisLabelFontFamily = 'Arial'
s1Display.PolarAxes.PolarAxisLabelFontFile = ''
s1Display.PolarAxes.PolarAxisLabelBold = 0
s1Display.PolarAxes.PolarAxisLabelItalic = 0
s1Display.PolarAxes.PolarAxisLabelShadow = 0
s1Display.PolarAxes.PolarAxisLabelFontSize = 12
s1Display.PolarAxes.LastRadialAxisTextOpacity = 1.0
s1Display.PolarAxes.LastRadialAxisTextFontFamily = 'Arial'
s1Display.PolarAxes.LastRadialAxisTextFontFile = ''
s1Display.PolarAxes.LastRadialAxisTextBold = 0
s1Display.PolarAxes.LastRadialAxisTextItalic = 0
s1Display.PolarAxes.LastRadialAxisTextShadow = 0
s1Display.PolarAxes.LastRadialAxisTextFontSize = 12
s1Display.PolarAxes.SecondaryRadialAxesTextOpacity = 1.0
s1Display.PolarAxes.SecondaryRadialAxesTextFontFamily = 'Arial'
s1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''
s1Display.PolarAxes.SecondaryRadialAxesTextBold = 0
s1Display.PolarAxes.SecondaryRadialAxesTextItalic = 0
s1Display.PolarAxes.SecondaryRadialAxesTextShadow = 0
s1Display.PolarAxes.SecondaryRadialAxesTextFontSize = 12
s1Display.PolarAxes.EnableDistanceLOD = 1
s1Display.PolarAxes.DistanceLODThreshold = 0.7
s1Display.PolarAxes.EnableViewAngleLOD = 1
s1Display.PolarAxes.ViewAngleLODThreshold = 0.7
s1Display.PolarAxes.SmallestVisiblePolarAngle = 0.5
s1Display.PolarAxes.PolarTicksVisibility = 1
s1Display.PolarAxes.ArcTicksOriginToPolarAxis = 1
s1Display.PolarAxes.TickLocation = 'Both'
s1Display.PolarAxes.AxisTickVisibility = 1
s1Display.PolarAxes.AxisMinorTickVisibility = 0
s1Display.PolarAxes.ArcTickVisibility = 1
s1Display.PolarAxes.ArcMinorTickVisibility = 0
s1Display.PolarAxes.DeltaAngleMajor = 10.0
s1Display.PolarAxes.DeltaAngleMinor = 5.0
s1Display.PolarAxes.PolarAxisMajorTickSize = 0.0
s1Display.PolarAxes.PolarAxisTickRatioSize = 0.3
s1Display.PolarAxes.PolarAxisMajorTickThickness = 1.0
s1Display.PolarAxes.PolarAxisTickRatioThickness = 0.5
s1Display.PolarAxes.LastRadialAxisMajorTickSize = 0.0
s1Display.PolarAxes.LastRadialAxisTickRatioSize = 0.3
s1Display.PolarAxes.LastRadialAxisMajorTickThickness = 1.0
s1Display.PolarAxes.LastRadialAxisTickRatioThickness = 0.5
s1Display.PolarAxes.ArcMajorTickSize = 0.0
s1Display.PolarAxes.ArcTickRatioSize = 0.3
s1Display.PolarAxes.ArcMajorTickThickness = 1.0
s1Display.PolarAxes.ArcTickRatioThickness = 0.5
s1Display.PolarAxes.Use2DMode = 0
s1Display.PolarAxes.UseLogAxis = 0

# reset view to fit data
renderView1.ResetCamera()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# show color bar/color legend
s1Display.SetScalarBarVisibility(renderView1, True)

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# Rescale transfer function
pLUT.RescaleTransferFunction(-7.349666595458984, 2.498786449432373)

# Rescale transfer function
pPWF.RescaleTransferFunction(-7.349666595458984, 2.498786449432373)

# create a new 'Slice'
slice1 = Slice(Input=s1)
slice1.SliceType = 'Plane'
slice1.HyperTreeGridSlicer = 'Plane'
slice1.UseDual = 0
slice1.Crinkleslice = 0
slice1.Triangulatetheslice = 1
slice1.Mergeduplicatedpointsintheslice = 1
slice1.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice1.SliceType.Origin = [-0.40398454666137695, -0.40404725074768066, -1.1920928955078125e-07]
slice1.SliceType.Normal = [1.0, 0.0, 0.0]
slice1.SliceType.Offset = 0.0

# init the 'Plane' selected for 'HyperTreeGridSlicer'
slice1.HyperTreeGridSlicer.Origin = [-0.40398454666137695, -0.40404725074768066, -1.1920928955078125e-07]
slice1.HyperTreeGridSlicer.Normal = [1.0, 0.0, 0.0]
slice1.HyperTreeGridSlicer.Offset = 0.0

# Properties modified on slice1.SliceType
slice1.SliceType.Origin = [-2.995, 0.0, 0.0]

# show data in view
slice1Display = Show(slice1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
slice1Display.Representation = 'Surface'
slice1Display.ColorArrayName = ['POINTS', 'p']
slice1Display.LookupTable = pLUT
slice1Display.MapScalars = 1
slice1Display.MultiComponentsMapping = 0
slice1Display.InterpolateScalarsBeforeMapping = 1
slice1Display.Opacity = 1.0
slice1Display.PointSize = 2.0
slice1Display.LineWidth = 1.0
slice1Display.RenderLinesAsTubes = 0
slice1Display.RenderPointsAsSpheres = 0
slice1Display.Interpolation = 'Gouraud'
slice1Display.Specular = 0.0
slice1Display.SpecularColor = [1.0, 1.0, 1.0]
slice1Display.SpecularPower = 100.0
slice1Display.Luminosity = 0.0
slice1Display.Ambient = 0.0
slice1Display.Diffuse = 1.0
slice1Display.Roughness = 0.3
slice1Display.Metallic = 0.0
slice1Display.Texture = None
slice1Display.RepeatTextures = 1
slice1Display.InterpolateTextures = 0
slice1Display.SeamlessU = 0
slice1Display.SeamlessV = 0
slice1Display.UseMipmapTextures = 0
slice1Display.BaseColorTexture = None
slice1Display.NormalTexture = None
slice1Display.NormalScale = 1.0
slice1Display.MaterialTexture = None
slice1Display.OcclusionStrength = 1.0
slice1Display.EmissiveTexture = None
slice1Display.EmissiveFactor = [1.0, 1.0, 1.0]
slice1Display.FlipTextures = 0
slice1Display.BackfaceRepresentation = 'Follow Frontface'
slice1Display.BackfaceAmbientColor = [1.0, 1.0, 1.0]
slice1Display.BackfaceOpacity = 1.0
slice1Display.Position = [0.0, 0.0, 0.0]
slice1Display.Scale = [1.0, 1.0, 1.0]
slice1Display.Orientation = [0.0, 0.0, 0.0]
slice1Display.Origin = [0.0, 0.0, 0.0]
slice1Display.Pickable = 1
slice1Display.Triangulate = 0
slice1Display.UseShaderReplacements = 0
slice1Display.ShaderReplacements = ''
slice1Display.NonlinearSubdivisionLevel = 1
slice1Display.UseDataPartitions = 0
slice1Display.OSPRayUseScaleArray = 0
slice1Display.OSPRayScaleArray = 'p'
slice1Display.OSPRayScaleFunction = 'PiecewiseFunction'
slice1Display.OSPRayMaterial = 'None'
slice1Display.Orient = 0
slice1Display.OrientationMode = 'Direction'
slice1Display.SelectOrientationVectors = 'U'
slice1Display.Scaling = 0
slice1Display.ScaleMode = 'No Data Scaling Off'
slice1Display.ScaleFactor = 0.09998312890529633
slice1Display.SelectScaleArray = 'p'
slice1Display.GlyphType = 'Arrow'
slice1Display.UseGlyphTable = 0
slice1Display.GlyphTableIndexArray = 'p'
slice1Display.UseCompositeGlyphTable = 0
slice1Display.UseGlyphCullingAndLOD = 0
slice1Display.LODValues = []
slice1Display.ColorByLODIndex = 0
slice1Display.GaussianRadius = 0.004999156445264817
slice1Display.ShaderPreset = 'Sphere'
slice1Display.CustomTriangleScale = 3
slice1Display.CustomShader = """ // This custom shader code define a gaussian blur
 // Please take a look into vtkSMPointGaussianRepresentation.cxx
 // for other custom shader examples
 //VTK::Color::Impl
   float dist2 = dot(offsetVCVSOutput.xy,offsetVCVSOutput.xy);
   float gaussian = exp(-0.5*dist2);
   opacity = opacity*gaussian;
"""
slice1Display.Emissive = 0
slice1Display.ScaleByArray = 0
slice1Display.SetScaleArray = ['POINTS', 'p']
slice1Display.ScaleArrayComponent = ''
slice1Display.UseScaleFunction = 1
slice1Display.ScaleTransferFunction = 'PiecewiseFunction'
slice1Display.OpacityByArray = 0
slice1Display.OpacityArray = ['POINTS', 'p']
slice1Display.OpacityArrayComponent = ''
slice1Display.OpacityTransferFunction = 'PiecewiseFunction'
slice1Display.DataAxesGrid = 'GridAxesRepresentation'
slice1Display.SelectionCellLabelBold = 0
slice1Display.SelectionCellLabelColor = [0.0, 1.0, 0.0]
slice1Display.SelectionCellLabelFontFamily = 'Arial'
slice1Display.SelectionCellLabelFontFile = ''
slice1Display.SelectionCellLabelFontSize = 18
slice1Display.SelectionCellLabelItalic = 0
slice1Display.SelectionCellLabelJustification = 'Left'
slice1Display.SelectionCellLabelOpacity = 1.0
slice1Display.SelectionCellLabelShadow = 0
slice1Display.SelectionPointLabelBold = 0
slice1Display.SelectionPointLabelColor = [1.0, 1.0, 0.0]
slice1Display.SelectionPointLabelFontFamily = 'Arial'
slice1Display.SelectionPointLabelFontFile = ''
slice1Display.SelectionPointLabelFontSize = 18
slice1Display.SelectionPointLabelItalic = 0
slice1Display.SelectionPointLabelJustification = 'Left'
slice1Display.SelectionPointLabelOpacity = 1.0
slice1Display.SelectionPointLabelShadow = 0
slice1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
slice1Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]
slice1Display.OSPRayScaleFunction.UseLogScale = 0

# init the 'Arrow' selected for 'GlyphType'
slice1Display.GlyphType.TipResolution = 6
slice1Display.GlyphType.TipRadius = 0.1
slice1Display.GlyphType.TipLength = 0.35
slice1Display.GlyphType.ShaftResolution = 6
slice1Display.GlyphType.ShaftRadius = 0.03
slice1Display.GlyphType.Invert = 0

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
slice1Display.ScaleTransferFunction.Points = [-0.00026925429119728506, 0.0, 0.5, 0.0, 0.03909657523036003, 1.0, 0.5, 0.0]
slice1Display.ScaleTransferFunction.UseLogScale = 0

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
slice1Display.OpacityTransferFunction.Points = [-0.00026925429119728506, 0.0, 0.5, 0.0, 0.03909657523036003, 1.0, 0.5, 0.0]
slice1Display.OpacityTransferFunction.UseLogScale = 0

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
slice1Display.DataAxesGrid.XTitle = 'X Axis'
slice1Display.DataAxesGrid.YTitle = 'Y Axis'
slice1Display.DataAxesGrid.ZTitle = 'Z Axis'
slice1Display.DataAxesGrid.XTitleFontFamily = 'Arial'
slice1Display.DataAxesGrid.XTitleFontFile = ''
slice1Display.DataAxesGrid.XTitleBold = 0
slice1Display.DataAxesGrid.XTitleItalic = 0
slice1Display.DataAxesGrid.XTitleFontSize = 12
slice1Display.DataAxesGrid.XTitleShadow = 0
slice1Display.DataAxesGrid.XTitleOpacity = 1.0
slice1Display.DataAxesGrid.YTitleFontFamily = 'Arial'
slice1Display.DataAxesGrid.YTitleFontFile = ''
slice1Display.DataAxesGrid.YTitleBold = 0
slice1Display.DataAxesGrid.YTitleItalic = 0
slice1Display.DataAxesGrid.YTitleFontSize = 12
slice1Display.DataAxesGrid.YTitleShadow = 0
slice1Display.DataAxesGrid.YTitleOpacity = 1.0
slice1Display.DataAxesGrid.ZTitleFontFamily = 'Arial'
slice1Display.DataAxesGrid.ZTitleFontFile = ''
slice1Display.DataAxesGrid.ZTitleBold = 0
slice1Display.DataAxesGrid.ZTitleItalic = 0
slice1Display.DataAxesGrid.ZTitleFontSize = 12
slice1Display.DataAxesGrid.ZTitleShadow = 0
slice1Display.DataAxesGrid.ZTitleOpacity = 1.0
slice1Display.DataAxesGrid.FacesToRender = 63
slice1Display.DataAxesGrid.CullBackface = 0
slice1Display.DataAxesGrid.CullFrontface = 1
slice1Display.DataAxesGrid.ShowGrid = 0
slice1Display.DataAxesGrid.ShowEdges = 1
slice1Display.DataAxesGrid.ShowTicks = 1
slice1Display.DataAxesGrid.LabelUniqueEdgesOnly = 1
slice1Display.DataAxesGrid.AxesToLabel = 63
slice1Display.DataAxesGrid.XLabelFontFamily = 'Arial'
slice1Display.DataAxesGrid.XLabelFontFile = ''
slice1Display.DataAxesGrid.XLabelBold = 0
slice1Display.DataAxesGrid.XLabelItalic = 0
slice1Display.DataAxesGrid.XLabelFontSize = 12
slice1Display.DataAxesGrid.XLabelShadow = 0
slice1Display.DataAxesGrid.XLabelOpacity = 1.0
slice1Display.DataAxesGrid.YLabelFontFamily = 'Arial'
slice1Display.DataAxesGrid.YLabelFontFile = ''
slice1Display.DataAxesGrid.YLabelBold = 0
slice1Display.DataAxesGrid.YLabelItalic = 0
slice1Display.DataAxesGrid.YLabelFontSize = 12
slice1Display.DataAxesGrid.YLabelShadow = 0
slice1Display.DataAxesGrid.YLabelOpacity = 1.0
slice1Display.DataAxesGrid.ZLabelFontFamily = 'Arial'
slice1Display.DataAxesGrid.ZLabelFontFile = ''
slice1Display.DataAxesGrid.ZLabelBold = 0
slice1Display.DataAxesGrid.ZLabelItalic = 0
slice1Display.DataAxesGrid.ZLabelFontSize = 12
slice1Display.DataAxesGrid.ZLabelShadow = 0
slice1Display.DataAxesGrid.ZLabelOpacity = 1.0
slice1Display.DataAxesGrid.XAxisNotation = 'Mixed'
slice1Display.DataAxesGrid.XAxisPrecision = 2
slice1Display.DataAxesGrid.XAxisUseCustomLabels = 0
slice1Display.DataAxesGrid.XAxisLabels = []
slice1Display.DataAxesGrid.YAxisNotation = 'Mixed'
slice1Display.DataAxesGrid.YAxisPrecision = 2
slice1Display.DataAxesGrid.YAxisUseCustomLabels = 0
slice1Display.DataAxesGrid.YAxisLabels = []
slice1Display.DataAxesGrid.ZAxisNotation = 'Mixed'
slice1Display.DataAxesGrid.ZAxisPrecision = 2
slice1Display.DataAxesGrid.ZAxisUseCustomLabels = 0
slice1Display.DataAxesGrid.ZAxisLabels = []
slice1Display.DataAxesGrid.UseCustomBounds = 0
slice1Display.DataAxesGrid.CustomBounds = [0.0, 1.0, 0.0, 1.0, 0.0, 1.0]

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
slice1Display.PolarAxes.Visibility = 0
slice1Display.PolarAxes.Translation = [0.0, 0.0, 0.0]
slice1Display.PolarAxes.Scale = [1.0, 1.0, 1.0]
slice1Display.PolarAxes.Orientation = [0.0, 0.0, 0.0]
slice1Display.PolarAxes.EnableCustomBounds = [0, 0, 0]
slice1Display.PolarAxes.CustomBounds = [0.0, 1.0, 0.0, 1.0, 0.0, 1.0]
slice1Display.PolarAxes.EnableCustomRange = 0
slice1Display.PolarAxes.CustomRange = [0.0, 1.0]
slice1Display.PolarAxes.PolarAxisVisibility = 1
slice1Display.PolarAxes.RadialAxesVisibility = 1
slice1Display.PolarAxes.DrawRadialGridlines = 1
slice1Display.PolarAxes.PolarArcsVisibility = 1
slice1Display.PolarAxes.DrawPolarArcsGridlines = 1
slice1Display.PolarAxes.NumberOfRadialAxes = 0
slice1Display.PolarAxes.AutoSubdividePolarAxis = 1
slice1Display.PolarAxes.NumberOfPolarAxis = 0
slice1Display.PolarAxes.MinimumRadius = 0.0
slice1Display.PolarAxes.MinimumAngle = 0.0
slice1Display.PolarAxes.MaximumAngle = 90.0
slice1Display.PolarAxes.RadialAxesOriginToPolarAxis = 1
slice1Display.PolarAxes.Ratio = 1.0
slice1Display.PolarAxes.PolarAxisColor = [1.0, 1.0, 1.0]
slice1Display.PolarAxes.PolarArcsColor = [1.0, 1.0, 1.0]
slice1Display.PolarAxes.LastRadialAxisColor = [1.0, 1.0, 1.0]
slice1Display.PolarAxes.SecondaryPolarArcsColor = [1.0, 1.0, 1.0]
slice1Display.PolarAxes.SecondaryRadialAxesColor = [1.0, 1.0, 1.0]
slice1Display.PolarAxes.PolarAxisTitleVisibility = 1
slice1Display.PolarAxes.PolarAxisTitle = 'Radial Distance'
slice1Display.PolarAxes.PolarAxisTitleLocation = 'Bottom'
slice1Display.PolarAxes.PolarLabelVisibility = 1
slice1Display.PolarAxes.PolarLabelFormat = '%-#6.3g'
slice1Display.PolarAxes.PolarLabelExponentLocation = 'Labels'
slice1Display.PolarAxes.RadialLabelVisibility = 1
slice1Display.PolarAxes.RadialLabelFormat = '%-#3.1f'
slice1Display.PolarAxes.RadialLabelLocation = 'Bottom'
slice1Display.PolarAxes.RadialUnitsVisibility = 1
slice1Display.PolarAxes.ScreenSize = 10.0
slice1Display.PolarAxes.PolarAxisTitleOpacity = 1.0
slice1Display.PolarAxes.PolarAxisTitleFontFamily = 'Arial'
slice1Display.PolarAxes.PolarAxisTitleFontFile = ''
slice1Display.PolarAxes.PolarAxisTitleBold = 0
slice1Display.PolarAxes.PolarAxisTitleItalic = 0
slice1Display.PolarAxes.PolarAxisTitleShadow = 0
slice1Display.PolarAxes.PolarAxisTitleFontSize = 12
slice1Display.PolarAxes.PolarAxisLabelOpacity = 1.0
slice1Display.PolarAxes.PolarAxisLabelFontFamily = 'Arial'
slice1Display.PolarAxes.PolarAxisLabelFontFile = ''
slice1Display.PolarAxes.PolarAxisLabelBold = 0
slice1Display.PolarAxes.PolarAxisLabelItalic = 0
slice1Display.PolarAxes.PolarAxisLabelShadow = 0
slice1Display.PolarAxes.PolarAxisLabelFontSize = 12
slice1Display.PolarAxes.LastRadialAxisTextOpacity = 1.0
slice1Display.PolarAxes.LastRadialAxisTextFontFamily = 'Arial'
slice1Display.PolarAxes.LastRadialAxisTextFontFile = ''
slice1Display.PolarAxes.LastRadialAxisTextBold = 0
slice1Display.PolarAxes.LastRadialAxisTextItalic = 0
slice1Display.PolarAxes.LastRadialAxisTextShadow = 0
slice1Display.PolarAxes.LastRadialAxisTextFontSize = 12
slice1Display.PolarAxes.SecondaryRadialAxesTextOpacity = 1.0
slice1Display.PolarAxes.SecondaryRadialAxesTextFontFamily = 'Arial'
slice1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''
slice1Display.PolarAxes.SecondaryRadialAxesTextBold = 0
slice1Display.PolarAxes.SecondaryRadialAxesTextItalic = 0
slice1Display.PolarAxes.SecondaryRadialAxesTextShadow = 0
slice1Display.PolarAxes.SecondaryRadialAxesTextFontSize = 12
slice1Display.PolarAxes.EnableDistanceLOD = 1
slice1Display.PolarAxes.DistanceLODThreshold = 0.7
slice1Display.PolarAxes.EnableViewAngleLOD = 1
slice1Display.PolarAxes.ViewAngleLODThreshold = 0.7
slice1Display.PolarAxes.SmallestVisiblePolarAngle = 0.5
slice1Display.PolarAxes.PolarTicksVisibility = 1
slice1Display.PolarAxes.ArcTicksOriginToPolarAxis = 1
slice1Display.PolarAxes.TickLocation = 'Both'
slice1Display.PolarAxes.AxisTickVisibility = 1
slice1Display.PolarAxes.AxisMinorTickVisibility = 0
slice1Display.PolarAxes.ArcTickVisibility = 1
slice1Display.PolarAxes.ArcMinorTickVisibility = 0
slice1Display.PolarAxes.DeltaAngleMajor = 10.0
slice1Display.PolarAxes.DeltaAngleMinor = 5.0
slice1Display.PolarAxes.PolarAxisMajorTickSize = 0.0
slice1Display.PolarAxes.PolarAxisTickRatioSize = 0.3
slice1Display.PolarAxes.PolarAxisMajorTickThickness = 1.0
slice1Display.PolarAxes.PolarAxisTickRatioThickness = 0.5
slice1Display.PolarAxes.LastRadialAxisMajorTickSize = 0.0
slice1Display.PolarAxes.LastRadialAxisTickRatioSize = 0.3
slice1Display.PolarAxes.LastRadialAxisMajorTickThickness = 1.0
slice1Display.PolarAxes.LastRadialAxisTickRatioThickness = 0.5
slice1Display.PolarAxes.ArcMajorTickSize = 0.0
slice1Display.PolarAxes.ArcTickRatioSize = 0.3
slice1Display.PolarAxes.ArcMajorTickThickness = 1.0
slice1Display.PolarAxes.ArcTickRatioThickness = 0.5
slice1Display.PolarAxes.Use2DMode = 0
slice1Display.PolarAxes.UseLogAxis = 0

# hide data in view
Hide(s1, renderView1)

# show color bar/color legend
slice1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# toggle 3D widget visibility (only when running from the GUI)
Hide3DWidgets(proxy=slice1.SliceType)

# reset view to fit data
renderView1.ResetCamera()

# set scalar coloring
ColorBy(slice1Display, ('POINTS', 's1'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(pLUT, renderView1)

# rescale color and/or opacity maps used to include current data range
slice1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
slice1Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 's1'
s1LUT = GetColorTransferFunction('s1')

# get opacity transfer function/opacity map for 's1'
s1PWF = GetOpacityTransferFunction('s1')

animationScene1.GoToLast()

# rescale color and/or opacity maps used to exactly fit the current data range
slice1Display.RescaleTransferFunctionToDataRange(False, True)


# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
s1LUT.ApplyPreset('Rainbow Uniform', True)

# Rescale transfer function
s1LUT.RescaleTransferFunction(0.0, 0.1)

# Rescale transfer function
s1PWF.RescaleTransferFunction(0.0, 0.1)

# get color legend/bar for s1LUT in view renderView1
s1LUTColorBar = GetScalarBar(s1LUT, renderView1)

# change scalar bar placement
s1LUTColorBar.WindowLocation = 'AnyLocation'
s1LUTColorBar.Position = [0.8577666874610106, 0.2897526501766784]
s1LUTColorBar.ScalarBarLength = 0.3300000000000006

# current camera placement for renderView1
renderView1.CameraPosition = [-5.252273617925049, 1.50009024143219, -8.435547351837158e-05]
renderView1.CameraFocalPoint = [-2.994999885559082, 1.50009024143219, -8.435547351837158e-05]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 0.7069127726546146

# save screenshot
SaveScreenshot('./image_01.png', renderView1, ImageResolution=[1603, 849],
    FontScaling='Scale fonts proportionally',
    OverrideColorPalette='WhiteBackground',
    StereoMode='No change',
    TransparentBackground=0, 
    # PNG options
    CompressionLevel='0')

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.CameraPosition = [-5.252273617925049, 1.50009024143219, -8.435547351837158e-05]
renderView1.CameraFocalPoint = [-2.994999885559082, 1.50009024143219, -8.435547351837158e-05]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 0.7069127726546146

#### uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).
